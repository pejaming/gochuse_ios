//
//  StringUtils.m
//  Gochuse
//
//  Created by 刘宁哲 on 15/3/14.
//
//

#import "StringUtils.h"

@implementation StringUtils

+ (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

@end
