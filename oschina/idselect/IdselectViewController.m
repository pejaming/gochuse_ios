//
//  IdselectViewController.m
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/29.
//
//

#import "IdselectViewController.h"

@interface IdselectViewController ()

@end

@implementation IdselectViewController
@synthesize headerView;
@synthesize motherButton;
@synthesize femaleButton;
@synthesize maleButton;
@synthesize mTitlebarLine;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNaviBarHide:true withAnimation:false];

    [self defaultNaviBarShowTitle:@"身份选择"];
    
    UIButton *leftBtn = [self defaultNaviBarLeftBtn];
    [leftBtn setImage:[UIImage imageNamed:@"web_back_press"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"" forState:UIControlStateNormal];
    
    [headerView setBackgroundColor:[Tool getColor:@"64bfa0"]];
    
    [maleButton setBackgroundImage:[UIImage imageNamed:@"idselect_male"] forState:UIControlStateNormal];
    maleButton.tag=1;
    [maleButton addTarget:self action:@selector(setIdType:) forControlEvents:UIControlEventTouchDown];
    
    [femaleButton setBackgroundImage:[UIImage imageNamed:@"idselect_female"] forState:UIControlStateNormal];
    femaleButton.tag=2;
    [femaleButton addTarget:self action:@selector(setIdType:) forControlEvents:UIControlEventTouchDown];
    
    [motherButton setBackgroundImage:[UIImage imageNamed:@"idselect_mother"] forState:UIControlStateNormal];
    motherButton.tag=3;
    [motherButton addTarget:self action:@selector(setIdType:) forControlEvents:UIControlEventTouchDown];
    
    mTitlebarLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"title_repeat"]];
    [mTitlebarLine setOpaque:NO];
    [[mTitlebarLine layer] setOpaque:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setIdType:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSString* idtypeValue;
    if(button.tag==1)
    {
        idtypeValue = id_male;
    }else  if(button.tag==2)
    {
        idtypeValue = id_female;
    }else if(button.tag==3)
    {
        idtypeValue = id_mother;
    }
    [[NSUserDefaults standardUserDefaults] setObject:idtypeValue forKey:idtypekey];
    [[NSUserDefaults standardUserDefaults] setObject:@"idselected" forKey:idselected];

       [self.navigationController popViewControllerAnimated:true];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
