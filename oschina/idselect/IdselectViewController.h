//
//  IdselectViewController.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/29.
//
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "JCNaviSubViewController.h"


@interface IdselectViewController : JCNaviSubViewController

@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIButton *maleButton;
@property (strong, nonatomic) IBOutlet UIButton *femaleButton;
@property (strong, nonatomic) IBOutlet UIButton *motherButton;
@property (strong, nonatomic) IBOutlet UIImageView *mTitlebarLine;

@end
