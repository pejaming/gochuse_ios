//入口类
#import "GochuseDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "CustomNavigationBar/CustomNavigationController.h"

@implementation GochuseDelegate

@synthesize window = _window;
@synthesize tabBarController = _tabBarController;
@synthesize membercenterViewController;
@synthesize homeViewController;
@synthesize searchViewController;

#pragma mark 程序生命周期
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [MobClick startWithAppkey:umeng_key reportPolicy:BATCH   channelId:@"80000"];//初始化uemeng统计
    
    NSString* idtypeValue = [[NSUserDefaults standardUserDefaults] objectForKey:idtypekey];
    if([StringUtils isBlankString:idtypeValue]){
    [[NSUserDefaults standardUserDefaults] setObject:id_female forKey:idtypekey];//如果用户没有进行过选择，默认选择女
    }
    
    //设置 UserAgent
    [ASIHTTPRequest setDefaultUserAgentString:[NSString stringWithFormat:@"%@/%@", [Tool getOSVersion], [Config Instance].getIOSGuid]];
    
    //显示系统托盘
    [application setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    //检查网络是否存在 如果不存在 则弹出提示
    [Config Instance].isNetworkRunning = [CheckNetwork isExistenceNetwork];
    
    
    //启动轮询  如果已经登录的话
    if ([Config Instance].isCookie) {
        [[MyThread Instance] startNotice];
    }
    
    [MyThread Instance].mainView = self.tabBarController.view;
    //准备未处理的异常
    [NdUncaughtExceptionHandler setDefaultHandler];
    
    //注册微信
    //[WXApi registerApp:@"wx41be5fe48092e94c"];
    
    [self initPush:launchOptions];
    
    [self initView];
    
    return YES;
}

-(void)initView
{
    
    //首页页面
    self.homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    JCNavigationController *homeNav = [[JCNavigationController alloc] initWithRootViewController:self.homeViewController];
    [homeViewController setNeedsStatusBarAppearanceUpdate];
    [homeNav navigationCanDragBack:true];
    
    //搜索页面
    self.searchViewController = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    JCNavigationController * searchNav = [[JCNavigationController alloc] initWithRootViewController:self.searchViewController];
    [searchNav navigationCanDragBack:true];

    
    //设置页
    self.membercenterViewController = [[MemberController alloc] initWithNibName:@"MemberController" bundle:nil];
    JCNavigationController * settingNav = [[JCNavigationController alloc] initWithRootViewController:self.membercenterViewController];
    [settingNav navigationCanDragBack:true];

    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    self.tabBarController = [[UITabBarController alloc] init];
    self.tabBarController.delegate = self;
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:
                                             homeNav,
                                             searchNav,
                                             settingNav,
                                             nil];
    
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setTintColor:[UIColor blackColor]];
    
    
    
    //初始化
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    
    
}

-(void)initPush:(NSDictionary *)launchOptions

{
    //设置推送
    [XGPush startApp:2200053357 appKey:@"I1AUM9V5U42M"];
    //[XGPush startApp:2290000353 appKey:@"key1"];
    
    //注销之后需要再次注册前的准备
    void (^successCallback)(void) = ^(void){
        
        NSLog(@"successCallback!!!");
        
        //如果变成需要注册状态
        if(![XGPush isUnRegisterStatus])
        {
            //iOS8注册push方法
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
            
            float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
            if(sysVer < 8){
                [self registerPush];
            }
            else{
                [self registerPushForIOS8];
            }
#else
            //iOS8之前注册push方法
            //注册Push服务，注册后才能收到推送
            [self registerPush];
#endif
        }
    };
    [XGPush initForReregister:successCallback];
    
    //推送反馈回调版本示例
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]handleLaunching's successBlock");
        [self jumpToBusi:launchOptions fromLaunching:YES];
        
        
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]handleLaunching's errorBlock");
    };
    
    //角标清0
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [XGPush handleLaunching:launchOptions successCallback:successBlock errorCallback:errorBlock];
}




- (void)applicationWillResignActive:(UIApplication *)application
{
    
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    //    NSLog(@"WillEnterForeground");
    //    [Config Instance].isNetworkRunning = [CheckNetwork isExistenceNetwork];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    [Config Instance].isNetworkRunning = [CheckNetwork isExistenceNetwork];
    if ([Config Instance].isNetworkRunning == NO) {
        UIAlertView *myalert = [[UIAlertView alloc] initWithTitle:@"警告" message:@"未连接网络,将使用离线模式" delegate:self cancelButtonTitle:@"确认" otherButtonTitles:nil,nil];
        [myalert show];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

#pragma mark UITab双击事件
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    int newTabIndex = self.tabBarController.selectedIndex;
    if (newTabIndex == m_lastTabIndex) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:Notification_TabClick object:[NSString stringWithFormat:@"%d", newTabIndex]];
    }
    else
    {
        m_lastTabIndex = newTabIndex;
    }
}

//微信相关
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [WXApi handleOpenURL:url delegate:self];
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [WXApi handleOpenURL:url delegate:self];
}


-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
        NSString *strTitle = [NSString stringWithFormat:@"分享到朋友圈"];
        
        NSString *strMsg = @"";
        if(resp.errCode == WXSuccess){
            strMsg = @"分享成功";
        }else if(resp.errCode == WXErrCodeUserCancel){
            strMsg = @"分享取消";
        }else{
            strMsg = [NSString stringWithFormat:@"分享失败，微信错误码：%d",resp.errCode];
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"map"]])
    {
        
    }
}





- (void)registerPushForIOS8{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE80_
    
    //Types
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    
    //Actions
    UIMutableUserNotificationAction *acceptAction = [[UIMutableUserNotificationAction alloc] init];
    
    acceptAction.identifier = @"ACCEPT_IDENTIFIER";
    acceptAction.title = @"Accept";
    
    acceptAction.activationMode = UIUserNotificationActivationModeForeground;
    acceptAction.destructive = NO;
    acceptAction.authenticationRequired = NO;
    
    //Categories
    UIMutableUserNotificationCategory *inviteCategory = [[UIMutableUserNotificationCategory alloc] init];
    
    inviteCategory.identifier = @"INVITE_CATEGORY";
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextDefault];
    
    [inviteCategory setActions:@[acceptAction] forContext:UIUserNotificationActionContextMinimal];
    
    NSSet *categories = [NSSet setWithObjects:inviteCategory, nil];
    
    
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:categories];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
}

- (void)registerPush{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}


+(void)handleReceiveNotification:(NSDictionary *)userInfo//app在运行时
{
    
}

+(void)handleLaunching:(NSDictionary *)launchOptions//app不在运行时，点击推送激活时
{
    NSString* type = [launchOptions objectForKey:@"type"];
    NSLog(@"%@push type= " , type);
}

//如果deviceToken获取不到会进入此事件
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSString *str = [NSString stringWithFormat: @"Error: %@",err];
    
    NSLog(@"%@",str);
    
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    //推送反馈(app运行时)
    [XGPush handleReceiveNotification:userInfo];
    
    
    // 回调版本示例
    
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        NSLog(@"[XGPush]handleReceiveNotification successBlock");
        
        if (userInfo)
            [self jumpToBusi:userInfo fromLaunching:NO];
        
        
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        NSLog(@"[XGPush]handleReceiveNotification errorBlock");
    };
    
    void (^completion)(void) = ^(void){
        // 失败之后的处理
        NSLog(@"[xg push completion]userInfo is %@",userInfo);
    };
    
    [XGPush handleReceiveNotification:userInfo successCallback:successBlock errorCallback:errorBlock completion:completion];
    
    //    NSDictionary *remoteNotify = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    //
    //    if (remoteNotify)
    //        [self jumpToBusi:remoteNotify fromLaunching:YES];
    
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken!!!!");
    NSString * deviceTokenStr = [XGPush registerDevice:deviceToken];
    
    void (^successBlock)(void) = ^(void){
        //成功之后的处理
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSLog(@"[XGPush]register successBlock ,deviceToken: %@",deviceTokenStr);
    };
    
    void (^errorBlock)(void) = ^(void){
        //失败之后的处理
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSLog(@"[XGPush]register errorBlock");
    };
    
    //注册设备
    [[XGSetting getInstance] setChannel:@"appstore"];
    //[[XGSetting getInstance] setGameServer:@"巨神峰"];
    [XGPush registerDevice:deviceToken successCallback:successBlock errorCallback:errorBlock];
    
    //如果不需要回调
    //[XGPush registerDevice:deviceToken];
    
    //打印获取的deviceToken的字符串
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken deviceTokenStr is %@",deviceTokenStr);
}


-(void)jumpToBusi:(NSDictionary *)pushinfo fromLaunching:(BOOL)isLaunching
{
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    NSString* action = [pushinfo objectForKey:@"a"];
    NSString* targetUrl = [pushinfo objectForKey:@"u"];
    NSString* showTitle = [pushinfo objectForKey:@"t"];
    
    NSLog(@"push action-----%@",action);
//    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:action message:targetUrl delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
//    // optional - add more buttons:
//    [alert addButtonWithTitle:@"Yes"];
//    [alert show];
    
    if([action_list  isEqual: action])
    {
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:action message:targetUrl delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
//        // optional - add more buttons:
//        [alert addButtonWithTitle:@"Yes"];
//        [alert show];
//        
//        NSLog(@"push targetUrl-----%@",targetUrl);
//        NSLog(@"push showTitle-----%@",showTitle);
//        
        GoodsListViewController *goodsListViewController = [[GoodsListViewController alloc] init];
        [goodsListViewController setTargetUrl:targetUrl];
        [goodsListViewController setShowTitle:showTitle];
        goodsListViewController.hidesBottomBarWhenPushed = YES;
        
        [homeNav pushViewController:goodsListViewController animated:YES];
        
    }else if([action_detail isEqual: action])
    {
        NSLog(@"push targetUrl-----%@",targetUrl);
        NSLog(@"push showTitle-----%@",showTitle);
        
        NSDictionary *item = [NSDictionary dictionaryWithObject:targetUrl forKey:@"taobaoChangeURL"];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [homeNav pushViewController:detailViewController animated:YES];
        
    }else if([action_link isEqual: action])
    {
        NSLog(@"push targetUrl-----%@",targetUrl);
        NSLog(@"push showTitle-----%@",showTitle);
        
        LinkViewController *linkViewController = [[LinkViewController alloc] init];
        NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                               targetUrl, @"taobaoChangeURL", showTitle, @"title", nil];
        linkViewController.hidesBottomBarWhenPushed = YES;
        linkViewController.item = item;
        [homeNav pushViewController:linkViewController animated:YES];
        
    }
}

@end
