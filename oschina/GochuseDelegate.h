//
//  OSAppDelegate.h
//  oschina
//
//  Created by wangjun on 12-3-1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSingleton.h"
#import "CheckNetwork.h"
#import "MemberController.h"
#import "SearchViewController.h"
#import "link/LinkViewController.h"

#import "NdUncaughtExceptionHandler.h"
#import "WXApi.h"
#import "HomeViewController.h"
#import "MobClick.h"
#import "XGPush.h"
#import "xgpush/XGSetting.h"
#import "JCNavigationController.h"
#import "StringUtils.h"
#import "Tool.h"
#import "MyThread.h"

@class ProfileBase;
@interface GochuseDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,WXApiDelegate>
{
    int m_lastTabIndex;
    JCNavigationController *homeNav;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UITabBarController *tabBarController;

@property (strong, nonatomic) HomeViewController *homeViewController;
@property (strong, nonatomic) MemberController * membercenterViewController;
@property (strong, nonatomic) SearchViewController *searchViewController;



@end
