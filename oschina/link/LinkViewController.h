//
//  DetailViewController.h
//  oschina
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import <UIKit/UIKit.h>
#import "CustomNaviBarView.h"
#import "CustomViewController.h"
#import "UtilityFunc.h"
#import "NJKWebViewProgress.h"
#import "NJKWebViewProgressView.h"
#import "JCNaviSubViewController.h"


@interface LinkViewController : JCNaviSubViewController<UIWebViewDelegate, NJKWebViewProgressDelegate>


@property (strong, nonatomic) IBOutlet UIWebView *mWebView;
@property (strong, nonatomic) IBOutlet UIImageView *mForwardButton;
@property (strong, nonatomic) IBOutlet UIImageView *mBackwardButton;
@property (strong, nonatomic) IBOutlet UIImageView *mRefreashButton;

@property (strong, nonatomic)  NSDictionary *item;

@property (strong, nonatomic) NJKWebViewProgressView *progressView;
@property (strong, nonatomic) NJKWebViewProgress *progressProxy;

@end
