//
//  DetailViewController.m
//  oschina
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import "LinkViewController.h"

@interface LinkViewController ()

@end

@implementation LinkViewController
@synthesize mWebView;
@synthesize item;
@synthesize mForwardButton;
@synthesize mBackwardButton;
@synthesize mRefreashButton;
@synthesize progressView;
@synthesize progressProxy;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:false];
    
    NSString *titleString = [item objectForKey:@"title"];
    [self defaultNaviBarShowTitle:titleString];
    
    UIButton *leftBtn = [self defaultNaviBarLeftBtn];
    [leftBtn setImage:[UIImage imageNamed:@"web_back_press"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"" forState:UIControlStateNormal];    //[self setNaviBarLeftBtn:nil];       // 若不需要默认的返回按钮，直接赋nil
    
    progressProxy = [[NJKWebViewProgress alloc] init];
    mWebView.delegate = progressProxy;
    progressProxy.webViewProxyDelegate = self;
    progressProxy.progressDelegate = self;
    
    
    CGFloat progressBarHeight = 1;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, 1, navigaitonBarBounds.size.width, progressBarHeight);
    progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    //progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    NSString *urlString = [item objectForKey:@"taobaoChangeURL"];
    NSLog(@"url = %@" ,urlString);
    
    NSURL *url =[NSURL URLWithString:urlString];
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [mWebView loadRequest:request];
    
    mForwardButton.tag=1;
    mForwardButton.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *forwardClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(functionButtonClick:)];
    [mForwardButton addGestureRecognizer:forwardClick];
    
    mBackwardButton.tag=2;
    mBackwardButton.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *backClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(functionButtonClick:)];
    [mBackwardButton addGestureRecognizer:backClick];
    
    mRefreashButton.tag=3;
    mRefreashButton.userInteractionEnabled=YES;
    
    UITapGestureRecognizer *refreshClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(functionButtonClick:)];
    [mRefreashButton addGestureRecognizer:refreshClick];
    
}

#pragma mark - NJKWebViewProgressDelegate
-(void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    [progressView setProgress:progress animated:YES];
    self.title = [mWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [progressView setFrame:CGRectMake(0, 64, 320, 1)];
    
    [self.view addSubview:progressView];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Remove progress view
    // because UINavigationBar is shared with other ViewControllers
    [progressView removeFromSuperview];
}



//查物流等功能的点击事件
- (void)functionButtonClick:(UITapGestureRecognizer*)tap
{
    UIView *button = [tap view];
    NSLog(@"functionButtonClick");
    
    if(button.tag == 1)
    {
        [mWebView goForward];
    }else if(button.tag == 2)
    {
        [mWebView goBack];
    }
    else if(button.tag == 3)
    {
        [mWebView reload];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
