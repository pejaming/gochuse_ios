//
//  StringUtils.h
//  Gochuse
//
//  Created by 刘宁哲 on 15/3/14.
//
//

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject
+ (BOOL) isBlankString:(NSString *)string;
@end
