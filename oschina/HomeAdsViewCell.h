//
//  HomeRecommendViewCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/10/25.
//
//

#import <UIKit/UIKit.h>

@interface HomeAdsViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *adsImg0;
@property (strong, nonatomic) IBOutlet UIImageView *adsImg1;
@property (strong, nonatomic) IBOutlet UIImageView *adsImg2;
@property (strong, nonatomic) IBOutlet UIImageView *adsImg3;

@property (strong, nonatomic) IBOutlet UIView *adsButton0;
@property (strong, nonatomic) IBOutlet UIView *adsButton1;
@property (strong, nonatomic) IBOutlet UIView *adsButton2;
@property (strong, nonatomic) IBOutlet UIView *adsButton3;

@property (strong, nonatomic) IBOutlet UILabel *adsLabel0;
@property (strong, nonatomic) IBOutlet UILabel *adsLabel1;
@property (strong, nonatomic) IBOutlet UILabel *adsLabel2;
@property (strong, nonatomic) IBOutlet UILabel *adsLabel3;


@property (strong, nonatomic) IBOutlet UILabel *adsDescLabel0;
@property (strong, nonatomic) IBOutlet UILabel *adsDescLabel1;
@property (strong, nonatomic) IBOutlet UILabel *adsDescLabel2;
@property (strong, nonatomic) IBOutlet UILabel *adsDescLabel3;
@end
