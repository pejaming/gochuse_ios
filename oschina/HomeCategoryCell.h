//
//  HomeCategoryCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/12/7.
//
//

#import <UIKit/UIKit.h>
#import "UIButton+WebCache.h"

@interface HomeCategoryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *button0;
@property (strong, nonatomic) IBOutlet UIView *button1;
@property (strong, nonatomic) IBOutlet UIView *button2;
@property (strong, nonatomic) IBOutlet UIView *button3;

@property (strong, nonatomic) IBOutlet UILabel *label0;
@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;

@property (strong, nonatomic) IBOutlet UIImageView *img0;
@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;



@end
