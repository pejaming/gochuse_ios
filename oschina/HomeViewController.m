#import "HomeViewController.h"


@interface HomeViewController ()
@end

@implementation HomeViewController
@synthesize mTitlebarImageView;
@synthesize homeTableView;
@synthesize catalog;
@synthesize mTitlebarLine;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self myInit];
    }
    return self;
}

- (void)myInit
{
    self.tabBarItem.image = [UIImage imageNamed:@"info"];
    self.tabBarItem.title = @"首页";
    self.view.backgroundColor =  [UIColor whiteColor];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initTableview];
    [self initGoodslist];
    
    //设置title文字颜色，目前有点问题，还没生效
    if([[[UIDevice currentDevice]systemVersion]floatValue]>=7.0){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    }
    [self setNeedsStatusBarAppearanceUpdate];

    //设置身份选择为空
    //[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:idselected];
    
    self.catalog=1;
    allCount = 0;

    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, -320.0f, self.view.frame.size.width, 320)];
        view.delegate = self;
        [self.homeTableView addSubview:view];
        _refreshHeaderView = view;
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    
    //先使用缓存
    NSDictionary *value = [Tool getCacheDic:5 andID:self.catalog];
    if(value !=nil)
    {
        [self parseDicToShow:value];
    }
    
    [self reload:YES];

}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"ss%@",[[NSUserDefaults standardUserDefaults]valueForKey:idselected]);
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:idselected] isEqualToString:@"idselected"]){
        
        [self.homeTableView setContentOffset:CGPointMake(0, -75) animated:YES];
        [self performSelector:@selector(doneManualRefresh) withObject:nil afterDelay:0.4];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:idselected];

    }
}


//展示更新数目toast
-(void)showUpdateGoodsCount
{
    
    if([todayCount isEqualToString:@""])
    {
        return;
    }
    CGRect rect = [[UIScreen mainScreen] bounds];
    UIView *toastView =[[ UIView alloc]initWithFrame:CGRectMake(0.0,200.0,250.0,40.0)];
    UILabel *toastContentView =[[ UILabel alloc]initWithFrame:CGRectMake(0.0,0.0,250.0,40.0)];
    toastView.alpha = 0.5;
    [toastView addSubview:toastContentView];
    toastContentView.layer.cornerRadius=5;
    toastContentView.textAlignment = UITextAlignmentCenter;

    toastContentView.layer.backgroundColor = [Tool getColor:@"56cfb9"].CGColor;
    [toastContentView setTextColor:[UIColor whiteColor]];
    [toastContentView setText:todayCount];

    toastView.center = CGPointMake(rect.size.width/2, 75);
    toastView.backgroundColor = [UIColor whiteColor];
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [window addSubview:toastView];
    
    [UIView animateWithDuration:0.8f animations:^{
        toastView.center = CGPointMake(rect.size.width/2, 90);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.8
                         animations:^{toastView.alpha = 0.0;}
                         completion:^(BOOL finished){ [toastView setHidden:YES]; }];
    }];

}



-(void)initTableview
{
    [[self navigationController] setNavigationBarHidden:YES animated:false];
    [self hideNaviBar:true];    // 隐藏标题
    [self setNaviBarLeftBtn:nil];       // 若不需要默认的返回按钮，直接赋nil
    [self.homeTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];//设置tableview无分割线
    self.homeTableView.backgroundColor = [UIColor whiteColor ];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshed:) name:Notification_TabClick object:nil];
    
    
    mTitlebarLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"title_repeat"]];
    [mTitlebarLine setOpaque:NO];
    [[mTitlebarLine layer] setOpaque:NO];
    
    [UtilityFunc resetScrlView:homeTableView contentInsetWithNaviBar:YES tabBar:NO iOS7ContentInsetStatusBarHeight:-1 inidcatorInsetStatusBarHeight:-1];
}

- (void)refreshed:(NSNotification *)notification
{
    if (notification.object) {
        if ([(NSString *)notification.object isEqualToString:@"0"]) {
            [self.homeTableView setContentOffset:CGPointMake(0, -75) animated:YES];
            [self performSelector:@selector(doneManualRefresh) withObject:nil afterDelay:0.4];

        }
    }
}
- (void)doneManualRefresh
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:self.homeTableView];
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:self.homeTableView];

}


//重新载入类型
- (void)reloadType:(int)ncatalog
{
    self.catalog = ncatalog;
    [self clear];
    [self.homeTableView reloadData];
    [self reload:NO];
}

//初始化商品列表
- (void)initGoodslist
{
    goodsList = [[NSMutableArray alloc] initWithCapacity:20];
    
    
    //这部分代码是为了让出首页list的前7个位置，用于添加类别，专题，广告，三段分割线和列表标题
    NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys :
                          @"name",@"name",
                          @"url",  @"url",
                          @"marking",@"marking", nil];
    
    for (int i=0; i<=6; i++) {
        [goodsList addObject:item];
    }
}

- (void)clear
{
    allCount = 0;
    [goodsList removeAllObjects];
    [self initGoodslist];
    isLoadOver = NO;
}
- (void)reload:(BOOL)noRefresh
{
    NSLog(@"reload");
    //如果有网络连接
    if ([Config Instance].isNetworkRunning) {
        if (isLoading || isLoadOver) {
            return;
        }
        if (!noRefresh) {
            allCount = 0;
        }
        int pageIndex = allCount/20;
        curPageIndex = pageIndex;
        NSString *urlStr = [NSString stringWithFormat:@"%@?p=%d", api_goods_index_list, pageIndex+1, 20];
        NSLog(@"url:%@", urlStr);
        NSURL *url = [NSURL URLWithString:urlStr];
        
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
        NSMutableURLRequest *request = [httpClient  requestWithMethod:@"GET" path:@"" parameters:nil ];
        
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
        AFJSONRequestOperation *operation =[AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                            
                                                                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, id responseObject) {
                                                                                               
                                                                                               NSData * jsonData = [self toJSONData:responseObject];
                                                                                               NSDictionary *resultDict = [jsonData objectFromJSONData];
                                                                                               
                                                                                               [self parseDicToShow:resultDict];                                                                                           }  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                                   
                                                                                                   NSLog(@"列表获取出错");
                                                                                                   NSLog(@"move failed:%@", [error localizedDescription]);
                                                                                                   
                                                                                                   //如果是刷新
                                                                                                   [self doneLoadingTableViewData];
                                                                                                   
                                                                                                   if ([Config Instance].isNetworkRunning == NO) {
                                                                                                       return;
                                                                                                   }
                                                                                                   isLoading = NO;
                                                                                                   if ([Config Instance].isNetworkRunning) {
                                                                                                       [Tool ToastNotification:networkError andView:self.view andLoading:NO andIsBottom:NO];
                                                                                                   }
                                                                                               }];
        isLoading = YES;
        [operation start];
        [self.homeTableView reloadData];
    }
    //如果没有网络连接
    else
    {
        NSString *value = [Tool getCache:5 andID:self.catalog];
        if (value) {
            NSMutableArray *newNews = [Tool readStrNewsArray:value andOld:goodsList];
            [self.homeTableView reloadData];
            isLoadOver = YES;
            [goodsList addObjectsFromArray:newNews];
            [self.homeTableView reloadData];
            [self doneLoadingTableViewData];
        }
    }
}


-(NSString*)DataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

//解析字典显示
-(void)parseDicToShow:(NSDictionary*)resultDict{
    NSArray *tmpGoodsList = [resultDict objectForKey:@"inf"];
    //  NSLog(@"%@",[self DataTOjsonString:resultDict]);
    categoryList = [resultDict objectForKey:@"category_list"];
    topicList = [resultDict objectForKey:@"topic_list"];
    adsList = [resultDict objectForKey:@"ads_list"];
    todayCount =[resultDict objectForKey:@"newinfo_count_str"];
    
    NSLog(@"tmpGoodsList:%d", [tmpGoodsList count]);
    isLoading = NO;
    if (_reloading) {
        [self clear];
    }
    
    @try {
        int count = [tmpGoodsList count];
        allCount += count;
        if (count < 20)
        {
            isLoadOver = YES;
        }
        [goodsList addObjectsFromArray:tmpGoodsList];
        [self.homeTableView reloadData];
        [self doneLoadingTableViewData];
        
        //如果是第一页 则缓存下来
        if (curPageIndex==0) {
            [Tool saveCacheDic:5 andID:self.catalog andDic:resultDict];
        }
    }
    @catch (NSException *exception) {
        [NdUncaughtExceptionHandler TakeException:exception];
    }
    @finally {
        [self doneLoadingTableViewData];
    }
}

- (NSData *)toJSONData:(id)theData{
    
    @try {
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                           options:NSJSONWritingPrettyPrinted
                                                             error: &error];
        
        if ([jsonData length] > 0 && error == nil){
            return jsonData;
        }else{
            return nil;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"exception %@",exception);
        return nil;
    }
    @finally {
        
    }
    
}

#pragma TableView的处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([Config Instance].isNetworkRunning) {
        if (isLoadOver) {
            return goodsList.count == 0 ? 1 : goodsList.count;
        }
        else
            return goodsList.count + 1;
    }
    else
        return goodsList.count;
}



// 设置单元格的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 62;
    }else if(indexPath.row == 1){
        return 5;
    }else if(indexPath.row == 2){
        return 135;
    }
    else if(indexPath.row == 3){
        return 5;
    }
    else if(indexPath.row == 4){
        return 140;
    }
    else if(indexPath.row == 5){
        return 5;
    } else if(indexPath.row == 6){
        return 30;
    }
    else if (indexPath.row  >= [goodsList count]) {
        return  50;
    }
    
    return 105;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // cell.backgroundColor = [Tool getCellBackgroundColor];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"indexPath row %d",[indexPath row]);
    if ([goodsList count] > 0) {
        if ([indexPath row] < [goodsList count])
        {
            
            if(indexPath.row == 0){
                HomeCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeCategoryCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeCategoryCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeCategoryCell class]]) {
                            cell = (HomeCategoryCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                if([categoryList count ]== 0)
                    return cell;
                
                if([categoryList count ]<4)
                    return cell;
                
                NSLog(@"[categoryList count ]%d",[categoryList count ]);
                
                
                NSDictionary *categoryItem = [categoryList objectAtIndex:0];
                cell.button0.tag = 0;
                [cell.label0 setText:[categoryItem objectForKey:@"category_name"]];
                [cell.img0 sd_setImageWithURL:[categoryItem objectForKey:@"img"]
                             placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                NSLog(@"[categoryList count ]%@",[categoryItem objectForKey:@"category_name"]);
                
                
                UITapGestureRecognizer *cateview0click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpCategoryList:)];
                [cell.button0 addGestureRecognizer:cateview0click];
                
                
                NSDictionary *categoryItem1 = [categoryList objectAtIndex:1];
                cell.button1.tag = 1;
                [cell.label1 setText:[categoryItem1 objectForKey:@"category_name"]];
                [cell.img1 sd_setImageWithURL:[categoryItem1 objectForKey:@"img"]
                             placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                UITapGestureRecognizer *cateview1click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpCategoryList:)];
                [cell.button1 addGestureRecognizer:cateview1click];
                
                
                NSDictionary *categoryItem2 = [categoryList objectAtIndex:2];
                cell.button2.tag = 2;
                [cell.label2 setText:[categoryItem2 objectForKey:@"category_name"]];
                [cell.img2 sd_setImageWithURL:[categoryItem2 objectForKey:@"img"]
                             placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                UITapGestureRecognizer *cateview2click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpCategoryList:)];
                [cell.button2 addGestureRecognizer:cateview2click];
                
                
                cell.button3.tag = 3;
                NSDictionary *categoryItem3 = [categoryList objectAtIndex:3];
                [cell.img3 sd_setImageWithURL:[categoryItem3 objectForKey:@"img"]
                             placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                
                //[cell.img3 setImage:[UIImage imageNamed:@"more"] ];
                UITapGestureRecognizer *cateview3click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpCategoryList:)];
                [cell.button3 addGestureRecognizer:cateview3click];
                
                
                return cell;
            } else if(indexPath.row==1)
            {
                //分隔符
                HomeSplitBarCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeSplitBarCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeSplitBarCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeSplitBarCell class]]) {
                            cell = (HomeSplitBarCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
            
            else if(indexPath.row==2)
            {
                //专题部分
                HomeTopicCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeBannerCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeTopicCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeTopicCell class]]) {
                            cell = (HomeTopicCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                if(iPhone5)
                {
                    //[cell.topicDescLabel0 setHidden:true];
                    [cell.topicDescLabel1 setHidden:true];
                    [cell.topicDescLabel2 setHidden:true];
                    [cell.topicDescLabel3 setHidden:true];
                }
                
                if([topicList count ]== 0)
                    return cell;
                
                if(iPhone5)
                {
                    [cell.topicDescLabel0 setHidden:true];
                    [cell.topicDescLabel1 setHidden:true];
                    [cell.topicDescLabel2 setHidden:true];
                    [cell.topicDescLabel3 setHidden:true];

                }
                
                NSDictionary *topic0 = [topicList objectAtIndex:0];
                cell.topicView0.tag = 0;
                [cell.topicLabel0 setText:[topic0 objectForKey:@"category_name"]];
               
                [cell.topicDescLabel0 setText:[topic0 objectForKey:@"cdesc"]];
                
                
                //[cell.rankButton setBackgroundImage:[UIImage imageNamed:@"hot1"] forState:UIControlStateNormal];
                UITapGestureRecognizer *topicview0click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpTopicList:)];
                [cell.topicView0 addGestureRecognizer:topicview0click];
                
                NSDictionary *topic1 = [topicList objectAtIndex:1];
                cell.topicView1.tag = 1;
                [cell.topicLabel1 setText:[topic1 objectForKey:@"category_name"]];
                
                [cell.topicDescLabel1 setText:[topic1 objectForKey:@"cdesc"]];
                
                
                //[cell.rankButton setBackgroundImage:[UIImage imageNamed:@"hot1"] forState:UIControlStateNormal];
                UITapGestureRecognizer *topicview1click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpTopicList:)];
                [cell.topicView1 addGestureRecognizer:topicview1click];
                
                NSDictionary *topic2 = [topicList objectAtIndex:2];
                cell.topicView2.tag = 2;
                [cell.topicLabel2 setText:[topic2 objectForKey:@"category_name"]];
                [cell.topicDescLabel2 setText:[topic2 objectForKey:@"cdesc"]];
                
                
                //[cell.rankButton setBackgroundImage:[UIImage imageNamed:@"hot1"] forState:UIControlStateNormal];
                UITapGestureRecognizer *topicview2click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpTopicList:)];
                [cell.topicView2 addGestureRecognizer:topicview2click];
                
                NSDictionary *topic3 = [topicList objectAtIndex:3];
                cell.topicView3.tag = 3;
                [cell.topicLabel3 setText:[topic3 objectForKey:@"category_name"]];
                [cell.topicDescLabel3 setText:[topic3 objectForKey:@"cdesc"]];
                
                
                //[cell.rankButton setBackgroundImage:[UIImage imageNamed:@"hot1"] forState:UIControlStateNormal];
                UITapGestureRecognizer *topicview3click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpTopicList:)];
                [cell.topicView3 addGestureRecognizer:topicview3click];
                
                return cell;
            }
            else if(indexPath.row==3)
            {
                //分隔符
                HomeSplitBarCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeSplitBarCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeSplitBarCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeSplitBarCell class]]) {
                            cell = (HomeSplitBarCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
            else if(indexPath.row==4)
            {
                //广告部分
                HomeAdsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeRecommendCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeAdsViewCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeAdsViewCell class]]) {
                            cell = (HomeAdsViewCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                
                if([adsList count ]== 0)
                    return cell;
                
                NSDictionary *ads0 = [adsList objectAtIndex:0];
                cell.adsButton0.tag = 0;
                [cell.adsLabel0 setText:[ads0 objectForKey:@"label"]];
                [cell.adsDescLabel0 setText:[ads0 objectForKey:@"cdesc"]];
                [cell.adsImg0 sd_setImageWithURL:[ads0 objectForKey:@"img"]
                                placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                UITapGestureRecognizer *cateview0click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpAdList:)];
                [cell.adsButton0 addGestureRecognizer:cateview0click];
                
                NSDictionary *ads1 = [adsList objectAtIndex:1];
                cell.adsButton1.tag = 1;
                [cell.adsLabel1 setText:[ads1 objectForKey:@"label"]];
                [cell.adsDescLabel1 setText:[ads1 objectForKey:@"cdesc"]];
                
                [cell.adsImg1 sd_setImageWithURL:[ads1 objectForKey:@"img"]
                                placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                UITapGestureRecognizer *cateview1click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpAdList:)];
                [cell.adsButton1 addGestureRecognizer:cateview1click];
                
                NSDictionary *ads2 = [adsList objectAtIndex:2];
                cell.adsButton2.tag = 2;
                [cell.adsLabel2 setText:[ads2 objectForKey:@"label"]];
                [cell.adsDescLabel2 setText:[ads2 objectForKey:@"cdesc"]];
                
                [cell.adsImg2 sd_setImageWithURL:[ads2 objectForKey:@"img"]
                                placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                UITapGestureRecognizer *cateview2click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpAdList:)];
                [cell.adsButton2 addGestureRecognizer:cateview2click];
                
                
                NSDictionary *ads3 = [adsList objectAtIndex:3];
                cell.adsButton3.tag = 3;
                [cell.adsLabel3 setText:[ads3 objectForKey:@"label"]];
                [cell.adsDescLabel3 setText:[ads3 objectForKey:@"cdesc"]];
                [cell.adsImg3 sd_setImageWithURL:[ads3 objectForKey:@"img"]
                                placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                UITapGestureRecognizer *cateview3click = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpAdList:)];
                [cell.adsButton3 addGestureRecognizer:cateview3click];
                
                
                return cell;
            }
            else if(indexPath.row==5)
            {
                //分隔符
                HomeSplitBarCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeSplitBarCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeSplitBarCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeSplitBarCell class]]) {
                            cell = (HomeSplitBarCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
            else if(indexPath.row==6)
            {
                //分隔符
                HomeGoodsTitleViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeGoodsTitleCellIdentifier];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HomeGoodsTitleViewCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[HomeGoodsTitleViewCell class]]) {
                            cell = (HomeGoodsTitleViewCell *)o;
                            break;
                        }
                    }
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
                
            }
            
            else{
                
                GoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
                [cell setAccessoryType:UITableViewCellAccessoryNone];
                if (!cell) {
                    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"GoodsCell" owner:self options:nil];
                    for (NSObject *o in objects) {
                        if ([o isKindOfClass:[GoodsCell class]]) {
                            cell = (GoodsCell *)o;
                            break;
                        }
                    }
                }
                
                @try {
                    // NSLog(@"index row %d",[indexPath row]);
                    //NSLog(@"goodlist count %d",[goodsList count]);
                    
                    if(goodsList == nil)
                        return cell ;
                    
                    NSDictionary *item = [goodsList objectAtIndex:[indexPath row]];//单个item的字典
                    
                    NSString *title = [item objectForKey:@"title"];//标题
                    NSString *nowPrice = [item objectForKey:@"lprice"];//现价
                    NSString *purchaseAmount = [item objectForKey:@"salenum"];//已购买数
                    NSString *originPrice = [item objectForKey:@"originallprice"];//原价
                    NSString *discount = [item objectForKey:@"zhekou"];//折扣
                    NSString *isbaoyou = [item objectForKey:@"isbaoyou"];//折扣
                    NSString *istmall = [item objectForKey:@"istmall"];//是否天猫
                    
                    
                    NSMutableString *nowPriceShow = [[NSMutableString alloc] initWithString:@"￥"];//用于显示的现价
                    [nowPriceShow appendFormat:nowPrice];
                    
                    NSMutableString *originPriceShow = [[NSMutableString alloc] initWithString:@"￥"];//用于显示的原价
                    [originPriceShow appendFormat:originPrice];
                    
                    
                    NSMutableString *purchaseAmountShow = [[NSMutableString alloc] initWithString:@""];//用于显示的购买人数
                    [purchaseAmountShow appendFormat:@"已售出"];
                    [purchaseAmountShow appendFormat:purchaseAmount];
                    [purchaseAmountShow appendFormat:@"件"];
                    
                    
                    
                    NSMutableString *discountShow = [[NSMutableString alloc] initWithString:discount];//用于显示的购买人数
                    [discountShow appendFormat:@"折"];
                    
                    //设置标题
                    cell.title.text = title;
                    cell.title.numberOfLines = 2;
                    
                    
                    //设置折扣
                    //  cell.discount.text = discountShow;
                    
                    [                    cell.discountButton .layer setMasksToBounds:YES];
                    [                    cell.discountButton .layer setCornerRadius:10.0]; //设置矩形四个圆角半径
                    [                    cell.discountButton .layer setBorderWidth:1.0]; //边框宽度
                    [                    cell.discountButton .layer setBorderColor:[Tool getColor:@"56cfb9"].CGColor]; //边框宽度
                    [                    cell.discountButton setTitle:discountShow forState:UIControlStateNormal];
                    
                    
                    
                    //设置现价
                    cell.nowPrice.text = nowPriceShow;
                    cell.nowPrice.font = [UIFont boldSystemFontOfSize:16];//设置粗体  正常的是 SystemFontOfSize
                    
                    //设置购买人数
                    cell.purchaseAmount.text = purchaseAmountShow;
                    
                    //是否包邮
                    if([isbaoyou isEqualToString:@"0"])
                    {
                        [cell.isbaoyouLabel setHidden:true];
                    }else{
                        [cell.isbaoyouLabel setHidden:false];
                        
                    }
                    
                    //是否天猫
                    if([istmall isEqualToString:@"0"])
                    {
                        [cell.isTmallImg setHidden:true];
                    }else{
                        [cell.isTmallImg setHidden:false];
                    }
                    
                    //设置原价
                    cell.originPrice.text = originPriceShow;
                    [self resizeLabel:cell.originPrice];
                    
                    NSURL *picUrlStr = [NSURL URLWithString:[item objectForKey:@"pic"]];
                    
                    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                    cell.imageView.clipsToBounds = YES;
                    
                    //[cell.imageView.layer setBorderWidth:0.5];
                    //[cell.imageView.layer setBorderColor:[Tool getColor:@"dedede"].CGColor];
                    [cell.imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
                   // cell.imageView.contentStretch=CGRectMake(0, 0, 50,50);
                    [cell.imageView sd_setImageWithURL:picUrlStr
                                      placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                    
                }
                
                
                // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                @catch (NSException *exception) {
                    [NdUncaughtExceptionHandler TakeException:exception];
                    NSLog(@"exception.name = %@" , exception.name);
                    NSLog(@"exception.reason = %@" , exception.reason);        }
                @finally {
                    // [self doneLoadingTableViewData];
                }
                
                return cell;
                
            }
            
        }
        else
        {
            return [[DataSingleton Instance] getLoadMoreCell:tableView andIsLoadOver:isLoadOver andLoadOverString:@"已经加载全部商品" andLoadingString:(isLoading ? loadingTip : loadNext20Tip) andIsLoading:isLoading];
        }
    }
    else
    {
        return [[DataSingleton Instance] getLoadMoreCell:tableView andIsLoadOver:isLoadOver andLoadOverString:@"已经加载全部商品                        " andLoadingString:(isLoading ? loadingTip : loadNext20Tip) andIsLoading:isLoading];
    }
    
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row <= 2 )
    {
        //return nil;
    }else
    {
        int row = [indexPath row];
        if (row >= [goodsList count]) {
            if (!isLoading) {
                [self performSelector:@selector(reload:)];
            }
        }
        else {
            NSDictionary *item = [goodsList objectAtIndex:[indexPath row]];
            
            item = [NSDictionary dictionaryWithObjectsAndKeys:
                    [item objectForKey:@"taobaoChangeURL"], @"taobaoChangeURL", @"详情", @"title", nil];
            DetailViewController *detailViewController = [[DetailViewController alloc] init];
            detailViewController.item =item;
            detailViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
        
    }
}




-(void)resizeLabel:(UILabel *)aLabel
{
    aLabel.lineBreakMode = UILineBreakModeWordWrap;
    aLabel.numberOfLines = 9999;
    
    CGSize aSize = [aLabel.text sizeWithFont:aLabel.font constrainedToSize:CGSizeMake(aLabel.frame.size.width, 9999.0f) lineBreakMode:UILineBreakModeWordWrap];
    
    aLabel.frame = CGRectMake(aLabel.frame.origin.x, aLabel.frame.origin.y, aSize.width, aSize.height+20.0f);
}

//跳转到专题
- (void)jumpTopicList:(UITapGestureRecognizer*)tap{
    NSLog(@"jumpTopicList..");
    UIView *clickView = [tap view];
    NSDictionary *categoryItem = [topicList objectAtIndex:clickView.tag];
    
    GoodsListViewController *goodsListViewController = [[GoodsListViewController alloc] init];
    [goodsListViewController setShowTitle:[categoryItem objectForKey:@"category_name"]];
    [goodsListViewController setTargetUrl:[categoryItem objectForKey:@"target_url"]];
    
    goodsListViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:goodsListViewController animated:YES];
    
}

//跳转到广告
-(void)jumpAdList:(UITapGestureRecognizer*)tap
{
    NSLog(@"jumpadlist..");
    
    UIView *clickView = [tap view];
    NSDictionary *categoryItem = [adsList objectAtIndex:clickView.tag];
    
    GoodsListViewController *goodsListViewController = [[GoodsListViewController alloc] init];
    [goodsListViewController setShowTitle:[categoryItem objectForKey:@"label"]];
    [goodsListViewController setTargetUrl:[categoryItem objectForKey:@"target_url"]];
    
    goodsListViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:goodsListViewController animated:YES];
    
    
}

//跳转到类别
-(void)jumpCategoryList:(UITapGestureRecognizer*)tap
{
    NSLog(@"jumpCategoryList..");
    UIView *temView = [tap view];
    NSInteger temIndex = temView.tag;
    
    
    if (temIndex == 3) {
        
        CategoryViewController *categoryViewController = [[CategoryViewController alloc] init];
        categoryViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:categoryViewController animated:YES];
    }else{
        
        if ([categoryList count]==0) {
            return;
        }
        
        NSDictionary *categoryItem = [categoryList objectAtIndex:temIndex];
        GoodsListViewController *goodsListViewController = [[GoodsListViewController alloc] init];
        //goodsListViewController = nil;
        [goodsListViewController setShowTitle:[categoryItem objectForKey:@"category_name"]];
        [goodsListViewController setTargetUrl:[categoryItem objectForKey:@"target_url"]];
        
        goodsListViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:goodsListViewController animated:YES];
    }
    
}

#pragma 下提刷新
- (void)reloadTableViewDataSource
{
    _reloading = YES;
}
- (void)doneLoadingTableViewData
{
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.homeTableView];
    if(curPageIndex ==1)
    {
        [self showUpdateGoodsCount];

    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    // NSLog(@"offset: %f", offset.y);
    // NSLog(@"content.height: %f", size.height);
    // NSLog(@"bounds.height: %f", bounds.size.height);
    // NSLog(@"inset.top: %f", inset.top);
    // NSLog(@"inset.bottom: %f", inset.bottom);
    // NSLog(@"pos: %f of %f", y, h);
    float reload_distance = 5;
    if(y > h + reload_distance) {
        //NSLog(@"load more rows");
        
        if(!isLoading)
            [self performSelector:@selector(reload:)];
    }
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    [self reloadTableViewDataSource];
    [self refresh];
}

- (void)egoRefreshTableHeaderDidTriggerToBottom
{
    if (!isLoading) {
        [self performSelector:@selector(reload:)];
    }
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view
{
    return _reloading;
}
- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view
{
    return [NSDate date];
}
- (void)refresh
{
    if ([Config Instance].isNetworkRunning) {
        isLoadOver = NO;
        [self reload:NO];
    }
    //无网络连接则读取缓存
    else {
        NSString *value = [Tool getCache:5 andID:self.catalog];
        if (value)
        {
            NSMutableArray *newNews = [Tool readStrNewsArray:value andOld:goodsList];
            if (newNews == nil) {
                [self.homeTableView reloadData];
            }
            else if(newNews.count <= 0){
                [self.homeTableView reloadData];
                isLoadOver = YES;
            }
            else if(newNews.count < 20){
                isLoadOver = YES;
            }
            [goodsList addObjectsFromArray:newNews];
            [self.homeTableView reloadData];
            [self doneLoadingTableViewData];
        }
    }
}


#pragma mark - status bar style
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}


- (void)viewDidUnload
{
    [self setHomeTableView:nil];
    _refreshHeaderView = nil;
    [goodsList removeAllObjects];
    goodsList = nil;
    [super viewDidUnload];
}

- (void)dealloc
{
    [self.homeTableView setDelegate:nil];
}

@end
