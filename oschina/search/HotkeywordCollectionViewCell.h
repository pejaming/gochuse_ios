//
//  HotkeywordCollectionViewCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/25.
//
//

#import <UIKit/UIKit.h>

@interface HotkeywordCollectionViewCell : UICollectionViewCell


@property (strong, nonatomic) IBOutlet UIImageView *hotkeywordImageView;
@property (strong, nonatomic) IBOutlet UIButton *hotkeywordButton;


@end
