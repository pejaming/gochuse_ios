//
//  SearchViewController.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/15.
//
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"
#import "HotkeywordCollectionViewCell.h"
#import "HotkeywordHeaderViewCell.h"
#import "HotkeywordCategoryTableViewCell.h"

#import "SearchViewController.h"
#import "CustomNaviBarView.h"
#import "CustomNaviBarSearchController.h"
#import "JCNaviSubViewController.h"
#import "LinkViewController.h"
#import "SearchNaviBarView.h"
#import "NSData+CommonCrypto.h"
#import "GoodsListViewController.h"



@interface SearchViewController : JCNaviSubViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource, UITableViewDelegate,CustomNaviBarSearchControllerDelegate>

- (IBAction)btnBack:(id)sender;

@property (strong, nonatomic) IBOutlet UICollectionView *hotkeywordCollectionView;
@property (strong, nonatomic) IBOutlet UITableView *hotkeywordCategoryTableView;

@property (nonatomic, readonly) CustomNaviBarSearchController *m_ctrlSearchBar;
@property (nonatomic, readonly) UIButton *m_btnNaviRightSearch;
@property (nonatomic, readonly) SearchNaviBarView *searchNaviBar;

@property (nonatomic, readonly) IBOutlet UISearchBar *searchBar;
@property (nonatomic, readonly) IBOutlet UIButton *searchButton;

@property (nonatomic,retain) NSArray *hotkeywordArray;
@property (nonatomic) int *nowSelectedHotkeycategory;

@end
