//
//  HotkeywordCategoryTableViewCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 15/1/1.
//
//

#import <UIKit/UIKit.h>

@interface HotkeywordCategoryTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *hotkeywordcategoryButton;
@property (strong, nonatomic) IBOutlet UIImageView *hotkeywordcategoryImageView;
//@property (strong, nonatomic) IBOutlet UILabel *hotkeywordcategoryRightLine;

@end
