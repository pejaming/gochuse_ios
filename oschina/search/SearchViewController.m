#import "SearchViewController.h"

@implementation SearchViewController

@synthesize m_ctrlSearchBar = _ctrlSearchBar;
@synthesize m_btnNaviRightSearch = _btnNaviRightSearch;
@synthesize hotkeywordCollectionView;
@synthesize hotkeywordArray;
@synthesize nowSelectedHotkeycategory;
@synthesize hotkeywordCategoryTableView;
@synthesize searchNaviBar;
@synthesize searchBar;
@synthesize searchButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self myInit];
    }
    return self;
}
- (void)myInit
{
    self.tabBarItem.image = [UIImage imageNamed:@"search"];
    self.tabBarItem.title = @"搜索";
}
- (void)viewDidAppear:(BOOL)animated
{
    [self setNaviBarHide:true withAnimation:false];

}
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    
  //  searchNaviBar = (SearchNaviBarView *)[SearchNaviBarView createNaviBarViewFromXIB];
   // [self replaceNaviBarView:searchNaviBar];
    
    
    [self.hotkeywordCollectionView registerClass:[HotkeywordCollectionViewCell class] forCellWithReuseIdentifier:@"hotkeycell"];//没有这行会崩溃
    //    [self.hotkeywordCollectionView registerClass:[HotkeywordHeaderViewCell class] forSupplementaryViewOfKind: UICollectionElementKindSectionHeader withReuseIdentifier:@"hotkeyheaderview"];//没有这行会崩溃
    
//    _btnNaviRightSearch = [CustomNaviBarView createImgNaviBarBtnByImgNormal:@"NaviBtn_Search" imgHighlight:@"NaviBtn_Search_H"  target:self action:@selector(btnSearch:)];
    
    //[self setNaviBarRightBtn:_btnNaviRightSearch];
    
    
    
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],UITextAttributeTextColor,[NSValue valueWithUIOffset:UIOffsetMake(0, 1)],UITextAttributeTextShadowOffset,nil] forState:UIControlStateNormal];
    
    NSString *hotkeywordPath=[[NSBundle mainBundle] pathForResource:@"hotkeyword" ofType:@"json"];
    // NSString *jsonContent=[[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSData *hotkeywordData = [NSData dataWithContentsOfFile:hotkeywordPath];
    hotkeywordArray=(NSArray *)[hotkeywordData mutableObjectFromJSONData];
    NSError *jsonError = nil;
    
    
    for(int i=0;i<hotkeywordArray.count;i++)
    {
        NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:i];
        NSString *cateimg=[hotkeywordCategory objectForKey:@"cateimg"];
    }
    
    nowSelectedHotkeycategory = 0;
    
    NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:0];
    NSString *name=[hotkeywordCategory objectForKey:@"name"];
    [[NSUserDefaults standardUserDefaults]setValue:name forKey:search_catetableview_cell_selected];
    
    NSIndexPath *ip=[NSIndexPath indexPathForRow:0 inSection:0];
    [hotkeywordCategoryTableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];

    [self initUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CustomNaviBar UI
- (void)initUI
{
    // [self setNaviBarTitle:@""];
    
    
    //    _ctrlSearchBar = [[CustomNaviBarSearchController alloc] initWithParentViewCtrl:self];
    //    _ctrlSearchBar.delegate = self;
    //    [_ctrlSearchBar resetPlaceHolder:@"寻找宝贝"];
    //    [_ctrlSearchBar showFixationSearchCtrl];
    //    [_ctrlSearchBar setRecentKeyword:@[@"时尚女包",@"羽绒服",@"雪地靴",@"毛呢外套",@"男鞋",@"时尚保温杯",@"进口零食",@"新款棉衣",@"秋冬四件套",@"秋冬童装",@"皮衣新款",@"毛衣打底裤"]];
    
    hotkeywordCategoryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    searchBar.delegate = self;
    //searchBar.backgroundColor=[UIColor clearColor];
    for (UIView *view in self.searchBar.subviews) {
        NSLog(@"searchBar.subviews%d",view.subviews.count);

        // for before iOS7.0
        if ([view isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [view removeFromSuperview];
            break;
        }
        // for later iOS7.0(include)
        if ([view isKindOfClass:NSClassFromString(@"UIView")] && view.subviews.count > 0) {
            [[view.subviews objectAtIndex:1] setBackgroundColor:[Tool getColor:@"f0f0f0" ]];
            [[view.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }

    [searchButton addTarget:self action:@selector(btnSearch:) forControlEvents:UIControlEventTouchUpInside];
    [searchButton.layer setMasksToBounds:YES];
    [searchButton.layer setCornerRadius:5.0];//设置矩形四个圆角半径
    
   
    
    
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    for(id cc in [searchBar.subviews[0] subviews])
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:@"取消" forState:UIControlStateNormal];
           
            [btn setBackgroundColor:[Tool getColor:@"56cfb9"]];
            [btn.layer setCornerRadius:5.0];
        }
    }
    [self.searchBar setPlaceholder:@""];// 搜索框的占位符

}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    [self.searchBar setPlaceholder:@"搜尽天下好物"];// 搜索框的占位符

    
}

- (void)btnSearch:(id)sender
{
//    if (!_ctrlSearchBar)
//    {
//        _ctrlSearchBar = [[CustomNaviBarSearchController alloc] initWithParentViewCtrl:self];
//        _ctrlSearchBar.delegate = self;
//    }else{}
//    [_ctrlSearchBar showTempSearchCtrl];
    
    NSString* strKeyword = [searchBar text];
    strKeyword = [strKeyword stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (strKeyword && strKeyword.length > 0)
    {
        NSString *encodeKeyword =  [strKeyword stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *url = @"http://ai.m.taobao.com/search.html?q=xxxx&pid=mm_98090984_9006731_30528926";
        url = [url stringByReplacingOccurrencesOfString:@"xxxx" withString:encodeKeyword];
        
        NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                               url, @"taobaoChangeURL", strKeyword, @"title", nil];
        LinkViewController *detailViewController = [[LinkViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else
    {
        //  _labelKeyword.text = @"";
    }
}

#pragma mark - CustomNaviBarSearchControllerDelegate
- (void)naviBarSearchCtrl:(CustomNaviBarSearchController *)ctrl searchKeyword:(NSString *)strKeyword
{
    if (strKeyword && strKeyword.length > 0)
    {
        // _labelKeyword.text = strKeyword;
        
        GoodsListViewController *goodsListViewController = [[GoodsListViewController alloc] init];
        [goodsListViewController setKey:strKeyword ];
        [goodsListViewController setShowTitle:strKeyword];
        
        //homeViewController.item =item;
        goodsListViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:goodsListViewController animated:YES];
    }
    else
    {
        //  _labelKeyword.text = @"";
    }
}

- (void)naviBarSearchCtrlCancel:(CustomNaviBarSearchController *)ctrl
{
    //_labelKeyword.text = @"";
}

- (void)naviBarSearchCtrlClearKeywordRecord:(CustomNaviBarSearchController *)ctrl
{
    [_ctrlSearchBar setRecentKeyword:nil];
}

/*
 * If you want to specify a height
 *
 
 
 -(float)heightForCellRowAtIndex:(NSUInteger)row forGridAtIndexPath:(NSIndexPath *)gridIndexPath
 {
 NSLog(@"call");
 return self.view.frame.size.width*1.2/3;
 }
 */


-(void)didSelectCellWithIndexPath:(NSIndexPath*) indexPath
{
    
    int index = [indexPath indexAtPosition:2]*3+[indexPath indexAtPosition:3];
    NSLog(@"index: %i",index);
    
    //    [[[UIAlertView alloc] initWithTitle:@"Tapped" message:[NSString stringWithFormat:@"You tapped cell %i in grid (%i,%i)",index,[indexPath indexAtPosition:0],[indexPath indexAtPosition:1]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
    
    [[[UIAlertView alloc] initWithTitle:@"Tapped" message:[NSString stringWithFormat:@"You tapped cell %i in grid (%i,%i)",index,[indexPath indexAtPosition:0],[indexPath indexAtPosition:1]] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
}


//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:self.nowSelectedHotkeycategory];
    NSArray *hotKeysList=[hotkeywordCategory objectForKey:@"hotKeysList"];
    NSDictionary *hotkeyword =[hotKeysList objectAtIndex:[indexPath row]];
    
    NSString *strKeyword=[hotkeyword objectForKey:@"key"];
    NSString *encodeKeyword =  [strKeyword stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *url = @"http://ai.m.taobao.com/search.html?q=xxxx&pid=mm_98090984_9006731_30528926";
    url = [url stringByReplacingOccurrencesOfString:@"xxxx" withString:encodeKeyword];

    
    NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                           url, @"taobaoChangeURL", strKeyword, @"title", nil];
    LinkViewController *detailViewController = [[LinkViewController alloc] init];
    detailViewController.item =item;
    detailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

//
//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
//
//
//    if (kind == UICollectionElementKindSectionHeader){
//
//        HotkeywordHeaderViewCell *cell = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"hotkeyheaderview" forIndexPath:indexPath];
//        if(cell == nil) {
//            NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HotkeywordHeaderViewCell" owner:self options:nil];
//            for (NSObject *o in objects) {
//                if ([o isKindOfClass:[HotkeywordHeaderViewCell class]]) {
//                    cell = (HotkeywordHeaderViewCell *)o;
//                    break;
//                }
//            }
//        }
//
//        cell.backgroundColor = [UIColor grayColor];
//        return cell;
//
//    }
//
//    return nil;
//}

//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * reuseIdentifier = @"hotkeycell";
    HotkeywordCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if(cell == nil) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HotkeywordCollectionViewCell" owner:self options:nil];
        for (NSObject *o in objects) {
            if ([o isKindOfClass:[HotkeywordCollectionViewCell class]]) {
                cell = (HotkeywordCollectionViewCell *)o;
                break;
            }
        }
    }
    
    NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:nowSelectedHotkeycategory];
    NSArray *hotKeysList=[hotkeywordCategory objectForKey:@"hotKeysList"];
    NSDictionary *hotkeyword=[hotKeysList objectAtIndex:[indexPath row]];
    NSString *title = [hotkeyword objectForKey:@"key"];
    NSString *imgUrl = [hotkeyword objectForKey:@"img"];
    
    
    [cell.hotkeywordButton setTitle:title forState:UIControlStateNormal];
    [cell.hotkeywordImageView sd_setImageWithURL:imgUrl
                                placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    
    
    //设置layer
   // CALayer *layer=[cell.hotkeywordImageView layer];
    //是否设置边框以及是否可见
    //[layer setMasksToBounds:YES];
    //设置边框圆角的弧度
    //[layer setCornerRadius:5.0];
    //设置边框线的宽
    //
    //[layer setBorderWidth:1];
    //设置边框线的颜色
    //[layer setBorderColor:[Tool getColor:@"dedede"].CGColor]; //边框宽度
    
    //
    //    NSDictionary *categoryDic = [categoryList objectAtIndex:[indexPath row]];//获取category对象字典
    //    NSString *categoryName = [categoryDic objectForKey:@"category_name"];//标题
    //    NSString *targetUrl = [categoryDic objectForKey:@"target_url"];//url
    //    NSString *imgUrl = [categoryDic objectForKey:@"img"];//url
    //
    //    // cell.categoryBtn.font = [UIFont boldSystemFontOfSize:11.0];
    //    [cell.categoryNameBtn setTitle:categoryName forState:UIControlStateNormal]  ;
    
    return cell;
}

//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:nowSelectedHotkeycategory];
    NSArray *hotKeysList=[hotkeywordCategory objectForKey:@"hotKeysList"];
    return [hotKeysList count ];
}


//每个section显示的标题

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    return @"";
    
}

//指定有多少个分区(Section)，默认为1

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}


//--------tableview


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.nowSelectedHotkeycategory = [indexPath row];
    [self.hotkeywordCollectionView reloadData];
    
    
    NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:[indexPath row]];
    NSString *name=[hotkeywordCategory objectForKey:@"name"];
    [[NSUserDefaults standardUserDefaults]setValue:name forKey:search_catetableview_cell_selected];
    
    
    [hotkeywordCategoryTableView reloadData];
}


//指定每个分区中有多少行，默认为1

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return hotkeywordArray.count;
}

//绘制Cell

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HotkeywordCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    if (!cell) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"HotkeywordCategoryTableViewCell" owner:self options:nil];
        for (NSObject *o in objects) {
            if ([o isKindOfClass:[HotkeywordCategoryTableViewCell class]]) {
                cell = (HotkeywordCategoryTableViewCell *)o;
                break;
            }
        }
    }
    
    NSLog(@"indexPath row] %d",[indexPath row] );
    
    NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:[indexPath row]];
    NSString *name=[hotkeywordCategory objectForKey:@"name"];
    NSString *cateimg=[hotkeywordCategory objectForKey:@"cateimg"];
    
    
    [cell.hotkeywordcategoryButton setText:name];
    [cell.hotkeywordcategoryImageView sd_setImageWithURL:cateimg
                                        placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:search_catetableview_cell_selected] isEqualToString:name])
    {
        cell.backgroundColor = [Tool getColor:@"7ddbcb"];
        cell.hotkeywordcategoryButton.textColor = [UIColor whiteColor];
    }
    else
    {
        cell.backgroundColor = [Tool getColor:@"fafafa"];
        cell.hotkeywordcategoryButton.textColor = [UIColor blackColor];
    }
    
    
    
    return cell;
    
}



-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger row = [indexPath row];
    return row;
}

//改变行的高度

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 47;
}


-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
//    [_dataSource removeAllObjects];
//    for(NSString * data in _dataBase)
//    {
//        if([data hasPrefix:searchBar.text])
//        {
//            [_dataSource addObject: data];
//        }
//    }
//    
//    [self.tableView reloadData];
//    [searchBar resignFirstResponder];
    [self btnSearch:@""];
}

//
//-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
//{
////    searchBar.text = @"";
////    [searchBar resignFirstResponder];
//}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
//    if(0 == searchText.length)
//    {
//        return ;
//        
//    }
//    
//    [_dataSource removeAllObjects];
//    for(NSString * str in _dataBase)
//    {
//        if([str hasPrefix:searchText])
//        {
//            [_dataSource addObject:str];
//        }
//    }
//    
//    [self.tableView reloadData];
}

@end
