//
//  MembercenterViewController.m
//  oschina
//
//  Created by 刘宁哲 on 14-5-24.
//
//

#import "MemberController.h"

@implementation MemberController
@synthesize mTableView;
@synthesize mButton;
@synthesize mImageView  ;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self myInit];
    }
    return self;
}
- (void)myInit
{
    self.tabBarItem.image = [UIImage imageNamed:@"active"];
    self.tabBarItem.title = @"个人中心";
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //mButton.frame = CGRectMake(30, 360, 90, 35);
    
    [mButton setTitle:@"点击" forState:UIControlStateNormal];
    [mButton addTarget:self action:@selector(zoomInAction:) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:mButton];
    
   // [mImageView setImage:[UIImage imageNamed:@"membercenter"]];
    
    
    // Set up the NSArray
    self.items = [[NSArray alloc] initWithObjects:@"用户反馈",@"检查更新",@"关于", nil];

//    UIBarButtoem *btnWeb = [[UIBarButtonItem alloc] initWithTitle:@"访问手机版" style:UIBarButtonItemStyleBordered target:self action:@selector(clickWeb:)];
//    self.navigationItem.rightBarButtonItem = btnWeb;
    
    
    
}
- (void)clickWeb:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://m.oschina.net"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count]; // or self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Step 1: Check to see if we can reuse a cell from a row that has just rolled off the screen
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    // Step 2: If there are no cells to reuse, create a new one
    if(cell == nil) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    // Add a detail view accessory
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    // Step 3: Set the cell text
    cell.textLabel.text = [self.items objectAtIndex:indexPath.row];
    
    // Step 4: Return the cell
    return cell;
}

 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     
     AboutViewController *aboutViewController = [[AboutViewController alloc] init];
     aboutViewController.hidesBottomBarWhenPushed = YES;
     [self.navigationController pushViewController:aboutViewController animated:YES];

 
// UIAlertView* showSelection;
// NSString* message;
// 
// 
// showSelection = [[UIAlertView alloc]
// initWithTitle:@"Selected"
// message:message
// delegate:nil
// cancelButtonTitle:@"OK"
// otherButtonTitles:nil];
// 
// [showSelection show];
 }

@end
