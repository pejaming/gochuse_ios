//
//  HomeBannerCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/10/25.
//
//

#import <UIKit/UIKit.h>

@interface HomeTopicCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UIImageView *topicImg0;
@property (strong, nonatomic) IBOutlet UIImageView *topicImg1;
@property (strong, nonatomic) IBOutlet UIImageView *topicImg2;
@property (strong, nonatomic) IBOutlet UIImageView *topicImg3;

@property (strong, nonatomic) IBOutlet UIView *topicView0;
@property (strong, nonatomic) IBOutlet UIView *topicView1;
@property (strong, nonatomic) IBOutlet UIView *topicView2;
@property (strong, nonatomic) IBOutlet UIView *topicView3;

@property (strong, nonatomic) IBOutlet UILabel *topicLabel0;
@property (strong, nonatomic) IBOutlet UILabel *topicLabel1;
@property (strong, nonatomic) IBOutlet UILabel *topicLabel2;
@property (strong, nonatomic) IBOutlet UILabel *topicLabel3;


@property (strong, nonatomic) IBOutlet UILabel *topicDescLabel0;
@property (strong, nonatomic) IBOutlet UILabel *topicDescLabel1;
@property (strong, nonatomic) IBOutlet UILabel *topicDescLabel2;
@property (strong, nonatomic) IBOutlet UILabel *topicDescLabel3;

@end
