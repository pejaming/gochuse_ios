//
//  UiTools.h
//  oschina
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import <Foundation/Foundation.h>
@class UserEntity;

@protocol PassValueDelegate <NSObject>

-(void)passValue:(UserEntity *)value;

@end
