//
//  TestView.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/17.
//
//

#import <UIKit/UIKit.h>

@interface TestView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
