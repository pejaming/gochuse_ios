//
//  MemberViewCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/29.
//
//

#import <UIKit/UIKit.h>

@interface MemberViewCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UIImageView *iconImg;
@property (strong, nonatomic) IBOutlet UIButton *listshowtextButton;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImg;


@end
