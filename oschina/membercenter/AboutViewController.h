//
//  AboutViewController.h
//  oschina
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import <UIKit/UIKit.h>
#import "JCNaviSubViewController.h"

@interface AboutViewController : JCNaviSubViewController

@property (strong, nonatomic) IBOutlet UIImageView *mImageView;

@end
