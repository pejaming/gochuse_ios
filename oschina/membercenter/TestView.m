//
//  TestView.m
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/17.
//
//

#import "TestView.h"

@implementation TestView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)awakeFromNib
{
    [[NSBundle mainBundle] loadNibNamed:@"TestView" owner:self options:nil];
    [self addSubview:self.contentView];
}

@end
