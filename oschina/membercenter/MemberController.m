//
//  MembercenterViewController.m
//  oschina
//
//  Created by 刘宁哲 on 14-5-24.
//
//

#import "MemberController.h"

@implementation MemberController

@synthesize mFunctionTableView;
@synthesize mButton;
@synthesize mtitleImgView;
@synthesize headerSexImg;
@synthesize mTaobaoFuntionView1;
@synthesize mTaobaoFuntionView2;
@synthesize mTaobaoFuntionView3;
@synthesize mTaobaoFuntionView4;
@synthesize items;
@synthesize iconpics;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self myInit];
    }
    return self;
}
- (void)myInit
{
    self.tabBarItem.image = [UIImage imageNamed:@"active"];
    self.tabBarItem.title = @"个人中心";
    self.view.backgroundColor =  [UIColor whiteColor];
    
}

-(void)initFunctionShow
{
    [mTaobaoFuntionView1 setTitle:@"查物流" forState:UIControlStateNormal];
    [mTaobaoFuntionView1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mTaobaoFuntionView1 setImage:[UIImage imageNamed:@"personcenter_check"] forState:UIControlStateNormal];
    [mTaobaoFuntionView1 setBackgroundColor:[Tool getColor:@"21bd99"]];
    [mTaobaoFuntionView1 setTitleEdgeInsets:UIEdgeInsetsMake(33, -32, 0, 0)];
    [mTaobaoFuntionView1 setImageEdgeInsets:UIEdgeInsetsMake(-15, 12, 0, 0)];
    mTaobaoFuntionView1.titleLabel.font    = [UIFont systemFontOfSize: 13];
    
    [mTaobaoFuntionView2 setTitle:@"购物车" forState:UIControlStateNormal];
    [mTaobaoFuntionView2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mTaobaoFuntionView2 setImage:[UIImage imageNamed:@"personcenter_buychart"] forState:UIControlStateNormal];
    [mTaobaoFuntionView2 setBackgroundColor:[Tool getColor:@"21bd99"]];
    [mTaobaoFuntionView2 setTitleEdgeInsets:UIEdgeInsetsMake(33, -32, 0, 0)];
    [mTaobaoFuntionView2 setImageEdgeInsets:UIEdgeInsetsMake(-15, 12, 0, 0)];
    mTaobaoFuntionView2.titleLabel.font    = [UIFont systemFontOfSize: 13];
    
    [mTaobaoFuntionView3 setTitle:@"看订单" forState:UIControlStateNormal];
    [mTaobaoFuntionView3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mTaobaoFuntionView3 setImage:[UIImage imageNamed:@"personcenter_order"] forState:UIControlStateNormal];
    [mTaobaoFuntionView3 setBackgroundColor:[Tool getColor:@"21bd99"]];
    [mTaobaoFuntionView3 setTitleEdgeInsets:UIEdgeInsetsMake(33, -32, 0, 0)];
    [mTaobaoFuntionView3 setImageEdgeInsets:UIEdgeInsetsMake(-15, 12, 0, 0)];
    mTaobaoFuntionView3.titleLabel.font    = [UIFont systemFontOfSize: 13];
    
    [mTaobaoFuntionView4 setTitle:@"找收藏" forState:UIControlStateNormal];
    [mTaobaoFuntionView4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [mTaobaoFuntionView4 setImage:[UIImage imageNamed:@"personcenter_collection"] forState:UIControlStateNormal];
    [mTaobaoFuntionView4 setBackgroundColor:[Tool getColor:@"21bd99"]];
    [mTaobaoFuntionView4 setTitleEdgeInsets:UIEdgeInsetsMake(33, -32, 0, 0)];
    [mTaobaoFuntionView4 setImageEdgeInsets:UIEdgeInsetsMake(-15, 12, 0, 0)];
    mTaobaoFuntionView4.titleLabel.font    = [UIFont systemFontOfSize: 13];
    
    [mTaobaoFuntionView1 addTarget:self action:@selector(functionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    mTaobaoFuntionView1.tag  =1 ;
    
    [mTaobaoFuntionView2 addTarget:self action:@selector(functionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    mTaobaoFuntionView2.tag  =2 ;
    
    [mTaobaoFuntionView3 addTarget:self action:@selector(functionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    mTaobaoFuntionView3.tag  =3 ;
    
    [mTaobaoFuntionView4 addTarget:self action:@selector(functionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    mTaobaoFuntionView4.tag  =4 ;
    
    [mButton addTarget:self action:@selector(functionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

//查物流等功能的点击事件
-(void)functionButtonClick:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSLog(@"functionButtonClick");
    
    if(button.tag == 1)
    {
        
        NSLog(@"functionButtonClick1");
        NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"http://h5.m.taobao.com/awp/mtb/mtb.htm?#!/awp/mtb/olist.htm?sta=5", @"taobaoChangeURL", @"查物流", @"title", nil];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if(button.tag == 2)
    {
        
        NSLog(@"functionButtonClick1");
        NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"http://h5.m.taobao.com/awp/base/cart.htm", @"taobaoChangeURL", @"购物车", @"title", nil];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if(button.tag == 3)
    {
        
        NSLog(@"functionButtonClick1");
        NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"http://h5.m.taobao.com/awp/mtb/mtb.htm?#!/awp/mtb/olist.htm?sta=4", @"taobaoChangeURL", @"看订单", @"title", nil];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }else if(button.tag == 4)
    {
        
        NSLog(@"functionButtonClick1");
        NSDictionary * item = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"http://h5.m.taobao.com/fav/index.htm", @"taobaoChangeURL", @"找收藏", @"title", nil];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:false];
    //[self hideNaviBar:true ];
    [self setNaviBarHide:true withAnimation:false];
    
    [self.mFunctionTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];//设置tableview无分割线
    
    [self initFunctionShow];//初始化功能区域显示
    
    NSString *hotkeywordPath=[[NSBundle mainBundle] pathForResource:@"hotkeyword" ofType:@"json"];
   // NSString *jsonContent=[[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSData *hotkeywordData = [NSData dataWithContentsOfFile:hotkeywordPath];
    NSArray *hotkeywordArray=(NSArray *)[hotkeywordData mutableObjectFromJSONData];
    NSError *jsonError = nil;
    
    for(int i=0;i<hotkeywordArray.count;i++)
    {
        NSDictionary *hotkeywordCategory=[hotkeywordArray objectAtIndex:i];
        NSString *cateimg=[hotkeywordCategory objectForKey:@"cateimg"];
    }
    
    // Set up the NSArray
    self.items = [[NSArray alloc] initWithObjects:@"身份选择",@"意见建议",@"关于我们", nil];
    
    self.iconpics = [[NSArray alloc] initWithObjects:@"personcenter_selectid",@"personcenter_feedback",@"personcenter_aboutus", nil];
    
    
    
    UITapGestureRecognizer *headseximgClick = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectsex:)];
    [headerSexImg addGestureRecognizer:headseximgClick];
}

- (void)viewDidAppear:(BOOL)animated
{
    
    NSString* idtypeValue = [[NSUserDefaults standardUserDefaults] objectForKey:idtypekey];
    if([idtypeValue isEqualToString:id_female]){
        [headerSexImg setImage:[UIImage imageNamed:@"girl"]];
        
    } else if([idtypeValue isEqualToString:id_male]){
        [headerSexImg setImage:[UIImage imageNamed:@"boy"]];
        
    } else if([idtypeValue isEqualToString:id_mother]){
        [headerSexImg setImage:[UIImage imageNamed:@"mama"]];
        
    }
}

- (void)clickWeb:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://m.oschina.net"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count]; // or self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MemberViewCell *cell = [tableView dequeueReusableCellWithIdentifier:HomeBannerCellIdentifier];
    if (!cell) {
        NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"MemberViewCell" owner:self options:nil];
        for (NSObject *o in objects) {
            if ([o isKindOfClass:[MemberViewCell class]]) {
                cell = (MemberViewCell *)o;
                break;
            }
        }
    }
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    //cell.textLabel.text = [self.items objectAtIndex:indexPath.row];
    [cell.listshowtextButton setTitle:[self.items objectAtIndex:indexPath.row] forState:(UIControlStateNormal)];
    [cell.iconImg setImage:[UIImage imageNamed:[self.iconpics objectAtIndex:indexPath.row]]];
    [cell.arrowImg setImage:[UIImage imageNamed:@"personcenter_arrow"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



// 设置单元格的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 60;
}



//跳转类别
- (void)selectsex:(UITapGestureRecognizer*)tap{
    NSLog(@"selectsex..");
    UIView *clickView = [tap view];
    IdselectViewController *idselectViewController = [[IdselectViewController alloc] init];

    idselectViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:idselectViewController animated:YES];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0)
    {
        IdselectViewController *idselectViewController = [[IdselectViewController alloc] init];
        //[goodsListViewController setKey:strKeyword ];
        //homeViewController.item =item;
        idselectViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:idselectViewController animated:YES];
    }else if ( indexPath.row==1){
        [UMFeedback showFeedback:self withAppkey:(NSString *)umeng_key];
//    }else if(indexPath.row==2)
//    {
//        [MobClick checkUpdate];
    }else{
        AboutViewController *aboutViewController = [[AboutViewController alloc] init];
        aboutViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:aboutViewController animated:YES];
    }
}

@end
