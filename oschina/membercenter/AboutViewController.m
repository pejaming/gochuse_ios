//
//  AboutViewController.m
//  oschina
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize mImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor =  [UIColor whiteColor];
    
    [self defaultNaviBarShowTitle:@"关于我们"];
    
    UIButton *leftBtn = [self defaultNaviBarLeftBtn];
    [leftBtn setImage:[UIImage imageNamed:@"web_back_press"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"" forState:UIControlStateNormal]; 
    [mImageView setImage:[UIImage imageNamed:@"wuyou_about_icon"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
