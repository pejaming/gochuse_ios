//
//  TaobaoFunctionView.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/15.
//
//

#import <UIKit/UIKit.h>


@protocol TaobaoFunctionDelegate <NSObject>

- (NSString *)getTitle;

@end


@interface TaobaoFunctionView : UIView
{
    NSString *title;
    NSString *picName;
    id <TaobaoFunctionDelegate> delegate;
    
}

@property (nonatomic, retain) id <TaobaoFunctionDelegate> delegate;


@property (strong,nonatomic) IBOutlet UIView *mContentView;
@property (strong,nonatomic) IBOutlet UILabel *mLabel;
@property (strong,nonatomic) IBOutlet UIButton *mButton;
@property (strong,nonatomic) IBOutlet UIImageView *mImageView;


@end
