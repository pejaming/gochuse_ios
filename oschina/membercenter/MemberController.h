//
//  MemberController.h
//  oschina
//
//  Created by 刘宁哲 on 14-5-28.
//
//

#import <UIKit/UIKit.h>
#import "AboutViewController.h"
#import "UMFeedback.h"
#import "TaobaoFunctionView.h"
#import "JCNaviSubViewController.h"
#import "MemberViewCell.h"
#import "IdselectViewController.h"

@interface MemberController : JCNaviSubViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>


@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UIImageView *headerSexImg;
@property (strong, nonatomic) IBOutlet UITableView *mFunctionTableView;

@property (strong, nonatomic) IBOutlet UILabel *mLabel;

@property (strong, nonatomic) IBOutlet UIButton *mButton;
@property (strong, nonatomic) IBOutlet UIImageView *mtitleImgView;


@property (strong, nonatomic) IBOutlet UIButton *mTaobaoFuntionView1;
@property (strong, nonatomic) IBOutlet UIButton *mTaobaoFuntionView2;
@property (strong, nonatomic) IBOutlet UIButton *mTaobaoFuntionView3;
@property (strong, nonatomic) IBOutlet UIButton *mTaobaoFuntionView4;

@property (nonatomic,retain) NSArray *hotkeywordArray;
@property (nonatomic,retain) NSArray *items;
@property (nonatomic,retain) NSArray *iconpics;



@end
