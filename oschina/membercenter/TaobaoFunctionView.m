//
//  TaobaoFunctionView.m
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/15.
//
//

#import "TaobaoFunctionView.h"

@implementation TaobaoFunctionView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */


- (void)awakeFromNib
{
    [[NSBundle mainBundle] loadNibNamed:@"TaobaoFunctionView" owner:self options:nil];
    [self addSubview:self.mContentView];
}

@end
