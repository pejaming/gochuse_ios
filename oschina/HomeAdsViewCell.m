//
//  HomeRecommendViewCell.m
//  Gochuse
//
//  Created by 刘宁哲 on 14/10/25.
//
//

#import "HomeAdsViewCell.h"

@implementation HomeAdsViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSString *)reuseIdentifier
{
    return HomeRecommendCellIdentifier;
}

@end
