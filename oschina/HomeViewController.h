//
//  HomeViewController.h
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/NSDictionary.h>
#import "GoodsCell.h"
#import "HomeAdsViewCell.h"
#import "HomeCategoryCell.h"
#import "HomeTopicCell.h"
#import "AFJSONRequestOperation.h"
#import "JSONKit.h"
#import "DetailViewController.h"
#import "PassValueDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "UIImageView+WebCache.h"
#import "GoodsListViewController.h"
#import "CustomNaviBarView.h"
#import "CustomNavigationBar/CustomViewController.h"
#import "UIButton+WebCache.h"
#import "HomeSplitBarCell.h"
#import "UtilityFunc.h"
#import "HomeGoodsTitleViewCell.h"

//PassValueDelegate 不知道是否有用，先留一下
@interface HomeViewController : CustomViewController<CLLocationManagerDelegate,UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    NSMutableArray *goodsList;
    NSMutableArray *categoryList;
    NSMutableArray *adsList;
    NSMutableArray *topicList;
    NSString *todayCount;

    
    //CLLocationManager* locationManager;
    // NSMutableArray * goodsList;
    BOOL isLoading;
    BOOL isLoadOver;
    int allCount;
    int curPageIndex;

    
    //下拉刷新
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    
}



@property (strong, nonatomic) IBOutlet UIView *mTitlebarView;
@property (strong, nonatomic) IBOutlet UIImageView *mTitlebarImageView;
@property (strong, nonatomic) IBOutlet UITableView *homeTableView;
@property (strong, nonatomic) IBOutlet UIImageView *mTitlebarLine;

//@property (retain, nonatomic) IBOutlet UILabel *longitudeText;
//@property (retain, nonatomic) IBOutlet UILabel *latituduText;
//@property (retain, nonatomic) IBOutlet UIActivityIndicatorView *activity;
//- (IBAction)findMe:(id)sender;
//- (IBAction)webMap:(id)sender;


@property int catalog;
- (void)reloadType:(int)ncatalog;
- (void)reload:(BOOL)noRefresh;

//清空
- (void)clear;

//下拉刷新
- (void)refresh;
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

@end
