#import "HomeViewController.h"
#import "UtilityFunc.h"


@interface GoodsListViewController ()
@end

@implementation GoodsListViewController
@synthesize goodsListTableView;
@synthesize m_btnNaviLeftBack;
@synthesize key;
@synthesize targetUrl;
@synthesize showTitle;
@synthesize loadingView;
@synthesize loadingImgView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [loadingView setHidden:false];
    [Tool rotate360DegreeWithImageView:loadingImgView];
    [goodsListTableView setHidden:true];
    
    
    [[self navigationController] setNavigationBarHidden:YES animated:false];
    
    [self defaultNaviBarShowTitle:showTitle];
    
    UIButton *leftBtn = [self defaultNaviBarLeftBtn];
    [leftBtn setImage:[UIImage imageNamed:@"web_back_press"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"" forState:UIControlStateNormal];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [UtilityFunc resetScrlView:goodsListTableView contentInsetWithNaviBar:YES tabBar:NO iOS7ContentInsetStatusBarHeight:-1 inidcatorInsetStatusBarHeight:-1];
    
    allCount = 0;
    //添加的代码
    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, -310.0f, self.view.frame.size.width, 320)];
        view.delegate = self;
        [self.goodsListTableView addSubview:view];
        _refreshHeaderView = view;
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    
    [self.goodsListTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];//设置tableview无分割线
    
    goodsList = [[NSMutableArray alloc] initWithCapacity:20];
    
    [self reload:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshed:) name:Notification_TabClick object:nil];
    
    
    //适配iOS7uinavigationbar遮挡tableView的问题
    if([[[UIDevice currentDevice]systemVersion]floatValue]>=7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self myInit];
    }
    return self;
}
- (void)myInit
{
}


- (void)refreshed:(NSNotification *)notification
{
    if (notification.object) {
        if ([(NSString *)notification.object isEqualToString:@"0"]) {
            [self.goodsListTableView setContentOffset:CGPointMake(0, -75) animated:YES];
            [self performSelector:@selector(doneManualRefresh) withObject:nil afterDelay:0.4];
        }
    }
}
- (void)doneManualRefresh
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:self.goodsListTableView];
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:self.goodsListTableView];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)viewDidUnload
{
    [self setGoodsListTableView:nil];
    _refreshHeaderView = nil;
    [goodsList removeAllObjects];
    goodsList = nil;
    [super viewDidUnload];
}

//重新载入类型
- (void)reloadType:(int)ncatalog
{
    self.catalog = ncatalog;
    [self clear];
    [self.goodsListTableView reloadData];
    [self reload:NO];
}

- (void)clear
{
    allCount = 0;
    //[self initGoodslist];
    [goodsList removeAllObjects];
    isLoadOver = NO;
}

- (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;}
    //    }
    //    if ([string isKindOfClass:[NSNull class]]) {
    //        return YES;
    //    }
    //    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
    //        return YES;
    //    }
    return NO;
}

- (void)reload:(BOOL)noRefresh
{
    NSLog(@"reload");
    
    
    //如果有网络连接
    if ([Config Instance].isNetworkRunning) {
        if (isLoading || isLoadOver) {
            return;
        }
        if (!noRefresh) {
            allCount = 0;
        }
        NSString *urlStr;
        
        @try {
            int pageIndex = allCount/20;
            // switch (self.catalog) {
            //     case 1:
            NSLog(@"targetUrl:   %@", targetUrl);
            
            
            if(![self isBlankString:targetUrl]) //如果传递过来的url不为空
            {
                targetUrl = [Tool ReplaceString:targetUrl useRegExp:@"&p=1" byString:@""] ;
                
                if([targetUrl rangeOfString:@"?"].location !=NSNotFound )////如果传递过来的url中包含？
                {
                    urlStr = [NSString stringWithFormat:@"%@&p=%d", targetUrl, pageIndex+1];//那么添加&
                }else{
                    urlStr = [NSString stringWithFormat:@"%@?p=%d", targetUrl, pageIndex+1];
                }
                NSLog(@"urlStr222:   %@", urlStr);
                
                
            }else
            {
                urlStr = [NSString stringWithFormat:@"%@?sk=%@&p=%d", api_goods_search_list, key, pageIndex+1];
                urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }
            
            NSLog(@"url1:   %@", urlStr);
            
        }
        @catch (NSException *e) {
            NSLog(@"Exception: %@", e);
            
        }
        @finally {
            
        }
        
        NSLog(@"url:   %@", urlStr);
        NSURL *url = [NSURL URLWithString:urlStr];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
        AFJSONRequestOperation *operation =[AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                            
                                                                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, id responseObject) {
                                                                                               
                                                                                               NSData * jsonData = [self toJSONData:responseObject];
                                                                                               NSDictionary *resultDict = [jsonData objectFromJSONData];
                                                                                               NSArray *tmpGoodsList = [resultDict objectForKey:@"inf"];
                                                                                               
                                                                                               NSLog(@"tmpGoodsList:%d", [tmpGoodsList count]);
                                                                                               
                                                                                               
                                                                                               isLoading = NO;
                                                                                               if (!noRefresh) {
                                                                                                   [self clear];
                                                                                               }
                                                                                               
                                                                                               @try {
                                                                                                   
                                                                                                   int count = [tmpGoodsList count];
                                                                                                   allCount += count;
                                                                                                   if (count < 20)
                                                                                                   {
                                                                                                       isLoadOver = YES;
                                                                                                   }
                                                                                                   [goodsList addObjectsFromArray:tmpGoodsList];
                                                                                                   [self.goodsListTableView reloadData];
                                                                                                   [self doneLoadingTableViewData];
                                                                                                   
                                                                                                   //如果是第一页 则缓存下来
                                                                                                   if (goodsList.count <= 20) {
                                                                                                       [Tool saveCache:5 andID:self.catalog andString:operation.responseString];
                                                                                                   }
                                                                                               }
                                                                                               @catch (NSException *exception) {
                                                                                                   [NdUncaughtExceptionHandler TakeException:exception];
                                                                                               }
                                                                                               @finally {
                                                                                                   [self doneLoadingTableViewData];
                                                                                               }
                                                                                           }  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                                                               
                                                                                               NSLog(@"列表获取出错");
                                                                                               　　                    NSLog(@"move failed:%@", [error localizedDescription]);
                                                                                               
                                                                                               //如果是刷新
                                                                                               [self doneLoadingTableViewData];
                                                                                               
                                                                                               if ([Config Instance].isNetworkRunning == NO) {
                                                                                                   return;
                                                                                               }
                                                                                               isLoading = NO;
                                                                                               if ([Config Instance].isNetworkRunning) {
                                                                                                   [Tool ToastNotification:networkError andView:self.view andLoading:NO andIsBottom:NO];
                                                                                               }
                                                                                           }];
        isLoading = YES;
        [operation start];
        [self.goodsListTableView reloadData];
        
        
        
    }
    //如果没有网络连接
    else
    {
        NSString *value = [Tool getCache:5 andID:self.catalog];
        if (value) {
            NSMutableArray *newNews = [Tool readStrNewsArray:value andOld:goodsList];
            [self.goodsListTableView reloadData];
            isLoadOver = YES;
            [goodsList addObjectsFromArray:newNews];
            [self.goodsListTableView reloadData];
            [self doneLoadingTableViewData];
        }
    }
}

- (NSData *)toJSONData:(id)theData{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: &error];
    
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}

#pragma TableView的处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([Config Instance].isNetworkRunning) {
        if (isLoadOver) {
            return goodsList.count == 0 ? 1 : goodsList.count;
        }
        else
            return goodsList.count + 1;
    }
    else
        return goodsList.count;
}



// 设置单元格的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    int row = [indexPath row];
    if (row >= [goodsList count]) {
        return  50;
    }
    return 105;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // cell.backgroundColor = [Tool getCellBackgroundColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([goodsList count] > 0) {
        if ([indexPath row] < [goodsList count])
        {
            
            
            GoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:NewsCellIdentifier];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
            if (!cell) {
                NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"GoodsCell" owner:self options:nil];
                for (NSObject *o in objects) {
                    if ([o isKindOfClass:[GoodsCell class]]) {
                        cell = (GoodsCell *)o;
                        break;
                    }
                }
            }
            
            @try {
                // NSLog(@"index row %d",[indexPath row]);
                //NSLog(@"goodlist count %d",[goodsList count]);
                
                if(goodsList == nil)
                    return cell ;
                
                NSDictionary *item = [goodsList objectAtIndex:[indexPath row]];//单个item的字典
                
                NSString *title = [item objectForKey:@"title"];//标题
                NSString *nowPrice = [item objectForKey:@"lprice"];//现价
                NSString *purchaseAmount = [item objectForKey:@"salenum"];//已购买数
                NSString *originPrice = [item objectForKey:@"originallprice"];//原价
                NSString *discount = [item objectForKey:@"zhekou"];//折扣
                NSString *isbaoyou = [item objectForKey:@"isbaoyou"];//折扣
                NSString *istmall = [item objectForKey:@"istmall"];//是否天猫
                
                
                NSMutableString *nowPriceShow = [[NSMutableString alloc] initWithString:@"￥"];//用于显示的现价
                [nowPriceShow appendFormat:nowPrice];
                
                NSMutableString *originPriceShow = [[NSMutableString alloc] initWithString:@"￥"];//用于显示的原价
                [originPriceShow appendFormat:originPrice];
                
                
                NSMutableString *purchaseAmountShow = [[NSMutableString alloc] initWithString:@""];//用于显示的购买人数
                [purchaseAmountShow appendFormat:@"已售出"];
                [purchaseAmountShow appendFormat:purchaseAmount];
                [purchaseAmountShow appendFormat:@"件"];
                
                NSMutableString *discountShow = [[NSMutableString alloc] initWithString:discount];//用于显示的购买人数
                [discountShow appendFormat:@"折"];
                
                //设置标题
                cell.title.text = title;
                cell.title.numberOfLines = 2;
                
                
                //设置折扣
                //  cell.discount.text = discountShow;
                
                [cell.discountButton .layer setMasksToBounds:YES];
                [cell.discountButton .layer setCornerRadius:10.0]; //设置矩形四个圆角半径
                [cell.discountButton .layer setBorderWidth:1.0]; //边框宽度
                [cell.discountButton .layer setBorderColor:[Tool getColor:@"56cfb9"].CGColor]; //边框宽度
                [cell.discountButton setTitle:discountShow forState:UIControlStateNormal];
                
                //设置现价
                cell.nowPrice.text = nowPriceShow;
                cell.nowPrice.font = [UIFont boldSystemFontOfSize:16];//设置粗体  正常的是 SystemFontOfSize
                
                //设置购买人数
                cell.purchaseAmount.text = purchaseAmountShow;
                
                //是否包邮
                if([isbaoyou isEqualToString:@"0"])
                {
                    [cell.isbaoyouLabel setHidden:true];
                }else{
                    [cell.isbaoyouLabel setHidden:false];
                    
                }
                
                //是否天猫
                if([istmall isEqualToString:@"0"])
                {
                    [cell.isTmallImg setHidden:true];
                }else{
                    [cell.isTmallImg setHidden:false];
                }
                
                //设置原价
                cell.originPrice.text = originPriceShow;
                [self resizeLabel:cell.originPrice];
                
                NSURL *picUrlStr = [NSURL URLWithString:[item objectForKey:@"pic"]];
                
                cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
                cell.imageView.clipsToBounds = YES;
                
                //[cell.imageView.layer setBorderWidth:0.5];
                //[cell.imageView.layer setBorderColor:[Tool getColor:@"dedede"].CGColor];
                [cell.imageView setContentScaleFactor:[[UIScreen mainScreen] scale]];
                cell.imageView.contentStretch=CGRectMake(0, 0, 50,50);
                [cell.imageView sd_setImageWithURL:picUrlStr
                                  placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
                
            }
            
            
            // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            @catch (NSException *exception) {
                [NdUncaughtExceptionHandler TakeException:exception];
                NSLog(@"exception.name = %@" , exception.name);
                NSLog(@"exception.reason = %@" , exception.reason);        }
            @finally {
                // [self doneLoadingTableViewData];
            }
            
            return cell;
            
            
            
        }
        else
        {
            return [[DataSingleton Instance] getLoadMoreCell:tableView andIsLoadOver:isLoadOver andLoadOverString:@"已经加载全部商品" andLoadingString:(isLoading ? loadingTip : loadNext20Tip) andIsLoading:isLoading];
        }
    }
    else
    {
        return [[DataSingleton Instance] getLoadMoreCell:tableView andIsLoadOver:isLoadOver andLoadOverString:@"已经加载全部商品                        " andLoadingString:(isLoading ? loadingTip : loadNext20Tip) andIsLoading:isLoading];
    }
}

-(void)resizeLabel:(UILabel *)aLabel
{
    aLabel.lineBreakMode = UILineBreakModeWordWrap;
    aLabel.numberOfLines = 9999;
    
    CGSize aSize = [aLabel.text sizeWithFont:aLabel.font constrainedToSize:CGSizeMake(aLabel.frame.size.width, 9999.0f) lineBreakMode:UILineBreakModeWordWrap];
    
    aLabel.frame = CGRectMake(aLabel.frame.origin.x, aLabel.frame.origin.y, aSize.width, aSize.height+20.0f);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int row = [indexPath row];
    if (row >= [goodsList count]) {
        if (!isLoading) {
            [self performSelector:@selector(reload:)];
        }
    }
    else {
        NSDictionary *item = [goodsList objectAtIndex:[indexPath row]];
        DetailViewController *detailViewController = [[DetailViewController alloc] init];
        detailViewController.item =item;
        detailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

#pragma 下提刷新
- (void)reloadTableViewDataSource
{
    _reloading = YES;
}
- (void)doneLoadingTableViewData
{
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.goodsListTableView];
    [goodsListTableView setHidden:false];
    [loadingView setHidden:true];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.bounds;
    CGSize size = scrollView.contentSize;
    UIEdgeInsets inset = scrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    // NSLog(@"offset: %f", offset.y);
    // NSLog(@"content.height: %f", size.height);
    // NSLog(@"bounds.height: %f", bounds.size.height);
    // NSLog(@"inset.top: %f", inset.top);
    // NSLog(@"inset.bottom: %f", inset.bottom);
    // NSLog(@"pos: %f of %f", y, h);
    float reload_distance = 5;
    if(y > h + reload_distance) {
        //NSLog(@"load more rows");
        
        if(!isLoading)
            [self performSelector:@selector(reload:)];
    }
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    [self reloadTableViewDataSource];
    [self refresh];
}

- (void)egoRefreshTableHeaderDidTriggerToBottom
{
    if (!isLoading) {
        [self performSelector:@selector(reload:)];
    }
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view
{
    return _reloading;
}
- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view
{
    return [NSDate date];
}
- (void)refresh
{
    if ([Config Instance].isNetworkRunning) {
        isLoadOver = NO;
        [self reload:NO];
    }
    //无网络连接则读取缓存
    else {
        NSString *value = [Tool getCache:5 andID:self.catalog];
        if (value)
        {
            NSMutableArray *newNews = [Tool readStrNewsArray:value andOld:goodsList];
            if (newNews == nil) {
                [self.goodsListTableView reloadData];
            }
            else if(newNews.count <= 0){
                [self.goodsListTableView reloadData];
                isLoadOver = YES;
            }
            else if(newNews.count < 20){
                isLoadOver = YES;
            }
            [goodsList addObjectsFromArray:newNews];
            [self.goodsListTableView reloadData];
            [self doneLoadingTableViewData];
        }
    }
}

- (void)dealloc
{
    [self.goodsListTableView setDelegate:nil];
}

@end
