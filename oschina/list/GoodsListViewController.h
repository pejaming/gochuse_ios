//
//  HomeViewController.h
//
//  Created by 刘宁哲 on 14-6-2.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/NSDictionary.h>
#import "GoodsCell.h"
#import "HomeAdsViewCell.h"
#import "HomeCategoryCell.h"
#import "HomeTopicCell.h"
#import "AFJSONRequestOperation.h"
#import "JSONKit.h"
#import "DetailViewController.h"
#import "PassValueDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import "UIImageView+WebCache.h"
#import "GoodsListViewController.h"
#import "CustomNaviBarView.h"
#import "CustomViewController.h"
#import "CategoryVIewController.h"
#import "JCNaviSubViewController.h"

@interface GoodsListViewController : JCNaviSubViewController<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    NSMutableArray *goodsList;
    NSString *key;
    BOOL isLoading;
    BOOL isLoadOver;
    int allCount;
    
    
    //下拉刷新
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    
}

@property (strong, nonatomic) IBOutlet UITableView *goodsListTableView;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIImageView *loadingImgView;

@property (nonatomic, readonly) UIButton *m_btnNaviLeftBack;
@property (strong, nonatomic)  NSString *key;
@property (strong, nonatomic)  NSString *targetUrl;
@property (strong, nonatomic)  NSString *showTitle;



//清空
- (void)clear;

@property int catalog;
- (void)reloadType:(int)ncatalog;
- (void)reload:(BOOL)noRefresh;
//下拉刷新
- (void)refresh;
- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;


@end
