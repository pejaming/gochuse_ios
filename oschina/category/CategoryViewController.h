//
//  CategoryViewController.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/18.
//
//

#import <UIKit/UIKit.h>
#import "JCNaviSubViewController.h"
#import "CategoryViewCell.h"
#import "GoodsListViewController.h"
#import "UIImageView+WebCache.h"

@interface CategoryViewController : JCNaviSubViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) IBOutlet UICollectionView *categoryCollectionView ;
@property (strong, nonatomic)  NSArray *categoryList;

@end
