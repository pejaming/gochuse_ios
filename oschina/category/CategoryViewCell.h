//
//  CategoryViewCell.h
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/22.
//
//

#import <UIKit/UIKit.h>

@interface CategoryViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *categoryImg;
@property (strong, nonatomic) IBOutlet UIButton *categoryNameBtn;


@end
