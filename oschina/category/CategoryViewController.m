//
//  CategoryViewController.m
//  Gochuse
//
//  Created by 刘宁哲 on 14/11/18.
//
//

#import "CategoryViewController.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController

@synthesize categoryCollectionView;
@synthesize categoryList;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:false];
    [self.categoryCollectionView registerClass:[CategoryViewCell class] forCellWithReuseIdentifier:@"GradientCell"];
 
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    [self defaultNaviBarShowTitle:@"分类"];
    
    UIButton *leftBtn = [self defaultNaviBarLeftBtn];
    [leftBtn setImage:[UIImage imageNamed:@"web_back_press"] forState:UIControlStateNormal];
    [leftBtn setTitle:@"" forState:UIControlStateNormal];    [self.categoryCollectionView setBackgroundColor:[UIColor whiteColor]];
    
    NSString *categoryPath=[[NSBundle mainBundle] pathForResource:@"category" ofType:@"json"];
    NSData *categoryData = [NSData dataWithContentsOfFile:categoryPath];
    NSDictionary* resultDict=(NSDictionary *)[categoryData mutableObjectFromJSONData];
    categoryList = [resultDict objectForKey:@"category_list"];


   // [self reload:YES];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSData *)toJSONData:(id)theData{
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error: &error];
    
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}
//
//- (void)reload:(BOOL)noRefresh
//{
//    NSLog(@"reload");
//    //如果有网络连接
//    if ([Config Instance].isNetworkRunning) {
//        
//        NSString *urlStr =  api_goods_category_list;
//        NSLog(@"url:%@", urlStr);
//        NSURL *url = [NSURL URLWithString:urlStr];
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//        
//        [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/plain"]];
//        AFJSONRequestOperation *operation =[AFJSONRequestOperation JSONRequestOperationWithRequest:request
//           success:^(NSURLRequest *request, NSHTTPURLResponse *response, id responseObject) {
//                  NSData * jsonData = [self toJSONData:responseObject];
//                  NSDictionary *resultDict = [jsonData objectFromJSONData];
//                  categoryList = [resultDict objectForKey:@"category_list"];
//                  NSLog(@"categoryList:%d", [categoryList count]);
//                  //isLoading = NO;
//                  @try {
//                   // int count = [tmpGoodsList count];
////                    allCount += count;
////                    if (count < 20)
////                       {
////                          isLoadOver = YES;
////                       }
//                  // [categoryList addObjectsFromArray:tmpGoodsList];
//                    NSLog(@"categoryList:%d", [categoryList count]);
//
//                   [self.categoryCollectionView reloadData];
//                   //[self doneLoadingTableViewData];
//                    //如果是第一页 则缓存下来
//                        if (categoryList.count <= 20) {
//                           [Tool saveCache:5 andID:1 andString:operation.responseString];
//                        }
//                    }
//                    @catch (NSException *exception) {
//                          [NdUncaughtExceptionHandler TakeException:exception];
//                    }
//                     @finally {
//                        //  [self doneLoadingTableViewData];
//                       }
//                     }  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
//                         NSLog(@"列表获取出错");
//                         NSLog(@"move failed:%@", [error localizedDescription]);
//                         //如果是刷新
//                        //[self doneLoadingTableViewData];
//                        if ([Config Instance].isNetworkRunning == NO) {
//                           return;
//                         }
//                         //isLoading = NO;
//                         if ([Config Instance].isNetworkRunning) {
//                               [Tool ToastNotification:@"错误 网络无连接" andView:self.view andLoading:NO andIsBottom:NO];
//                          }
//                    }];
//       // isLoading = YES;
//        [operation start];
//        [self.categoryCollectionView reloadData];
//    }
//    //如果没有网络连接
//    else
//    {
//        NSString *value = [Tool getCache:5 andID:1];
//        if (value) {
//            NSMutableArray *newNews = [Tool readStrNewsArray:value andOld:categoryList];
//            [self.categoryCollectionView reloadData];
//            //isLoadOver = YES;
//            //[goodsList addObjectsFromArray:newNews];
//            [self.categoryCollectionView reloadData];
//            //[self doneLoadingTableViewData];
//        }
//    }
//}
//
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect f = [[UIScreen mainScreen] applicationFrame];
    
        CGFloat t;
        t = f.size.width/3-0.7;
        f.size.width = f.size.height;
        f.size.height = t;
    return CGSizeMake(t, 100);
}

//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
       
    NSDictionary *categoryDic = [categoryList objectAtIndex:[indexPath row]];//获取category对象字典
    NSString *targetUrl = [categoryDic objectForKey:@"target_url"];//url
    NSString *categoryName = [categoryDic objectForKey:@"category_name"];//标题

    GoodsListViewController *goodsListViewController = [[GoodsListViewController alloc] init];
    [goodsListViewController setTargetUrl:targetUrl];
    [goodsListViewController setShowTitle:categoryName];

    //homeViewController.item =item;
    goodsListViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:goodsListViewController animated:YES];

}


//每个UICollectionView展示的内容
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * reuseIdentifier = @"GradientCell";
    CategoryViewCell * cell = [categoryCollectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if(cell == nil) {
            NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"CategoryViewCell" owner:self options:nil];
                for (NSObject *o in objects) {
                    if ([o isKindOfClass:[CategoryViewCell class]]) {
                        cell = (CategoryViewCell *)o;
                        break;
                    }
                }
            }

    
    //cell.backgroundColor = [UIColor redColor];
    
    NSDictionary *categoryDic = [categoryList objectAtIndex:[indexPath row]];//获取category对象字典
    NSString *categoryName = [categoryDic objectForKey:@"category_name"];//标题
    NSString *targetUrl = [categoryDic objectForKey:@"target_url"];//url
    NSString *imgUrl = [categoryDic objectForKey:@"bimg"];//url
    
   // cell.categoryBtn.font = [UIFont boldSystemFontOfSize:11.0];
    NSLog(@"imgUrl %@ " ,imgUrl);
    [cell.categoryNameBtn setTitle:categoryName forState:UIControlStateNormal]  ;
    //[cell.categoryNameBtn setImage:[UIImage imageNamed:@"icon"] forState:UIControlStateNormal];
    
    [cell.categoryImg sd_setImageWithURL:imgUrl
                    placeholderImage:[UIImage imageNamed:@"picdefault"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    
    [cell.layer setBorderWidth:0.5];
    [cell.layer setBorderColor:[Tool getColor:@"f2f2f2"].CGColor]; //边框宽度


    return cell;
}

//定义展示的UICollectionViewCell的个数
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSLog(@"%dcategoryList size", [categoryList count]);
    return [categoryList count];
}
@end
